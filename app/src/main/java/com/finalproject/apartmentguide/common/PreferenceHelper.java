package com.finalproject.apartmentguide.common;

import android.content.Context;

import com.finalproject.apartmentguide.model.User;
 
public interface PreferenceHelper {
    void setUserInformation(User user);

    void setAccessToken(String token);

    User getUserInformation();

    String getAccessToken();
}
