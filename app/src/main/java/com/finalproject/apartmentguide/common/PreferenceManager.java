package com.finalproject.apartmentguide.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.finalproject.apartmentguide.core.dagger.PreferenceInfo;
import com.finalproject.apartmentguide.model.User;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Singleton;
 
@Singleton
public class PreferenceManager implements PreferenceHelper {
    private static final String PREF_KEY_CURRENT_USER = "PREF_CURRENT_USER";
    private static final String PREF_KEY_CURRENT_TOKEN = "PREF_CURRENT_TOKEN";
    Context context;
    private SharedPreferences mPrefs;
    private Gson gson;

    @Inject
    public PreferenceManager(Context context, Gson gson, @PreferenceInfo String prefFileName) {
        this.context = context;
        this.gson = gson;
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public void setUserInformation(User user) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER, gson.toJson(user)).apply();
    }

    @Override
    public void setAccessToken(String token) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_TOKEN, token).apply();
    }

    @Override
    public User getUserInformation() {
        String userString = mPrefs.getString(PREF_KEY_CURRENT_USER, "user");
        if (!userString.equals("user")) {
            return gson.fromJson(userString, User.class);
        }
        return null;
    }

    @Override
    public String getAccessToken() {
        return null;
    }
}
