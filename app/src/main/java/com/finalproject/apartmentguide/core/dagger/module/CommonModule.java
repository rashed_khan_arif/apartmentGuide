package com.finalproject.apartmentguide.core.dagger.module;

import android.content.Context;

import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.common.PreferenceManager;
import com.finalproject.apartmentguide.config.Constant;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.core.dagger.PreferenceInfo;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
 
@Module
public class CommonModule {
    @Singleton
    @Provides
    public APGApplication provideAPGApplication() {
        return APGApplication.getDefault();
    }

    @Singleton
    @Provides
    public Context provideContext() {
        return APGApplication.getDefault();
    }

    @PreferenceInfo
    @Provides
    public String providePreferenceName() {
        return Constant.PREF_FILE;
    }

    @Singleton
    @Provides
    public PreferenceHelper providePreferenceManager(PreferenceManager preferenceManager) {
        return preferenceManager;
    }
}
