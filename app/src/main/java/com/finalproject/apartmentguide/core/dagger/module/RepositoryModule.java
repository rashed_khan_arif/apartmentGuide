package com.finalproject.apartmentguide.core.dagger.module;

import android.content.Context;

import com.finalproject.apartmentguide.data.Service.AdvertisementService;
import com.finalproject.apartmentguide.data.Service.ApartmentService;
import com.finalproject.apartmentguide.data.Service.AuthenticationService;
import com.finalproject.apartmentguide.data.Service.CommonService;
import com.finalproject.apartmentguide.data.Service.UserApiService;
import com.finalproject.apartmentguide.data.repository.AdvertisementRepository;
import com.finalproject.apartmentguide.data.repository.ApartmentRepository;
import com.finalproject.apartmentguide.data.repository.AuthenticationRepo;
import com.finalproject.apartmentguide.data.repository.CommonRepository;
import com.finalproject.apartmentguide.data.repository.UserRepository;
import com.finalproject.apartmentguide.data.repository.impl.AdvertisementRepositoryImpl;
import com.finalproject.apartmentguide.data.repository.impl.ApartmentRepositoryImpl;
import com.finalproject.apartmentguide.data.repository.impl.AuthenticationRepoImpl;
import com.finalproject.apartmentguide.data.repository.impl.CommonRepositoryImpl;
import com.finalproject.apartmentguide.data.repository.impl.UserRepoImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
 
@Module
public class RepositoryModule {
    public RepositoryModule() {
    }

    @Singleton
    @Provides
    public AuthenticationRepo provideAuthRepository(AuthenticationService authenticationService, Context context) {
        return new AuthenticationRepoImpl(authenticationService, context);
    }

    @Singleton
    @Provides
    public UserRepository provideUserRepository(UserApiService userApiService, Context context) {
        return new UserRepoImpl(userApiService, context);
    }

    @Singleton
    @Provides
    public AdvertisementRepository provideAdvertisementRepository(AdvertisementService advertisementService, Context context) {
        return new AdvertisementRepositoryImpl(advertisementService, context);
    }

    @Singleton
    @Provides
    public CommonRepository provideCommonRepository(CommonService commonService, Context context) {
        return new CommonRepositoryImpl(commonService, context);
    }

    @Singleton
    @Provides
    public ApartmentRepository provideAptRepo(ApartmentService service) {
        return new ApartmentRepositoryImpl(service);
    }
}
