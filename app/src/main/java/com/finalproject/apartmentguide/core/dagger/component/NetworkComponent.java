package com.finalproject.apartmentguide.core.dagger.component;

import com.finalproject.apartmentguide.core.dagger.module.CommonModule;
import com.finalproject.apartmentguide.core.dagger.module.NetworkModule;
import com.finalproject.apartmentguide.core.dagger.module.RepositoryModule;
import com.finalproject.apartmentguide.core.dagger.module.ServiceModule;

import javax.inject.Singleton;

import dagger.Component;
 
@Component(modules = {NetworkModule.class, ServiceModule.class, RepositoryModule.class, CommonModule.class})
public interface NetworkComponent {

}
