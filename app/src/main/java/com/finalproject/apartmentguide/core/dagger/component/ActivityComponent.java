package com.finalproject.apartmentguide.core.dagger.component;

import com.finalproject.apartmentguide.core.dagger.PerActivity;
import com.finalproject.apartmentguide.core.dagger.module.ActivityModule;
import com.finalproject.apartmentguide.ui.account.LoginActivity;

import javax.inject.Singleton;

import dagger.Component;

 
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {


}
