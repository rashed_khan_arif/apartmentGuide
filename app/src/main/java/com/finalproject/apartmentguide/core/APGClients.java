package com.finalproject.apartmentguide.core;

import com.finalproject.apartmentguide.data.ApiService;

import retrofit2.Retrofit;
 
public class APGClients {
    private static APGClients clients;

    private APGClients() {
    }

    public static synchronized APGClients getInstance() {
        if (clients == null) {
            clients = new APGClients();
        }
        return clients;
    }



}
