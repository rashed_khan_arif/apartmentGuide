package com.finalproject.apartmentguide.core.dagger.component;

import android.content.Context;

import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.core.dagger.module.ActivityModule;
import com.finalproject.apartmentguide.core.dagger.module.ApplicationModule;
import com.finalproject.apartmentguide.core.dagger.module.CommonModule;
import com.finalproject.apartmentguide.core.dagger.module.NetworkModule;
import com.finalproject.apartmentguide.core.dagger.module.RepositoryModule;
import com.finalproject.apartmentguide.core.dagger.module.ServiceModule;
import com.finalproject.apartmentguide.ui.account.LoginActivity;
import com.finalproject.apartmentguide.ui.account.SignUpActivity;
import com.finalproject.apartmentguide.ui.account.VerificationActivity;
import com.finalproject.apartmentguide.ui.advertisement.AdvertisementActivity;
import com.finalproject.apartmentguide.ui.advertisement.AdvertisementListActivity;
import com.finalproject.apartmentguide.ui.advertisement.apartment_details.AdvertisementDetailsActivity;
import com.finalproject.apartmentguide.ui.advertisement.apartment_details.ApartmentDetailsActivity;
import com.finalproject.apartmentguide.ui.advertisement.post_advertisement.CreateAdvertisementActivity;
import com.finalproject.apartmentguide.ui.advertisement.post_advertisement.CreateApartmentActivity;
import com.finalproject.apartmentguide.ui.home.ChooseOptActivity;
import com.finalproject.apartmentguide.ui.user.UserActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ActivityModule.class, NetworkModule.class, ServiceModule.class, RepositoryModule.class, ApplicationModule.class, CommonModule.class})
public interface ApplicationComponent {
    void inject(APGApplication apgApplication);

    void inject(LoginActivity loginActivity);

    void inject(SignUpActivity signUpActivity);

    void inject(AdvertisementActivity advertisementActivity);

    void inject(AdvertisementDetailsActivity advertisementDetailsActivity);

    void inject(CreateAdvertisementActivity createAdvertisementActivity);

    void inject(ChooseOptActivity chooseOptActivity);

    void inject(UserActivity userActivity);

    void inject(AdvertisementListActivity advertisementListActivity);

    void inject(ApartmentDetailsActivity apartmentDetailsActivity);

    void inject(CreateApartmentActivity createApartmentActivity);

    void inject(VerificationActivity verificationActivity);
}
