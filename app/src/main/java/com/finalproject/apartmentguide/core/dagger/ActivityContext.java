package com.finalproject.apartmentguide.core.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Qualifier;
 
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityContext {
}
