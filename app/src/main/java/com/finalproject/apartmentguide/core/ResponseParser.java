package com.finalproject.apartmentguide.core;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.HttpException;

 

public class ResponseParser {
    public static String getErrorMessageFromResponse(Throwable throwable) {
        String response = null;
        try {
            ResponseBody responseBody = ((HttpException) throwable).response().errorBody();
            if (responseBody == null)
                response = String.valueOf(((Exception) throwable));
            else
                response = responseBody.string();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
