package com.finalproject.apartmentguide.core.dagger.module;

import com.finalproject.apartmentguide.ui.account.LoginMvpPresenter;
import com.finalproject.apartmentguide.ui.account.LoginMvpView;
import com.finalproject.apartmentguide.ui.account.LoginPresenter;
import com.finalproject.apartmentguide.ui.account.SignUpPresenter;
import com.finalproject.apartmentguide.ui.account.SignUpMvpView;
import com.finalproject.apartmentguide.ui.account.SignUpMvpPresenter;
import com.finalproject.apartmentguide.ui.advertisement.AdvertisementMvpPresenter;
import com.finalproject.apartmentguide.ui.advertisement.AdvertisementMvpView;
import com.finalproject.apartmentguide.ui.advertisement.AdvertisementPresenter;
import com.finalproject.apartmentguide.ui.advertisement.apartment_details.AdvertisementDetailsMvpPresenter;
import com.finalproject.apartmentguide.ui.advertisement.apartment_details.AdvertisementDetailsMvpView;
import com.finalproject.apartmentguide.ui.advertisement.apartment_details.ApartmentDetailsPresenter;
import com.finalproject.apartmentguide.ui.advertisement.post_advertisement.CreateAdMvpPresenter;
import com.finalproject.apartmentguide.ui.advertisement.post_advertisement.CreateAdMvpView;
import com.finalproject.apartmentguide.ui.advertisement.post_advertisement.CreateAdPresenter;
import com.finalproject.apartmentguide.ui.common.CommonMvpPresenter;
import com.finalproject.apartmentguide.ui.common.CommonMvpView;
import com.finalproject.apartmentguide.ui.common.CommonPresenter;
import com.finalproject.apartmentguide.ui.user.UserMvpPresenter;
import com.finalproject.apartmentguide.ui.user.UserMvpView;
import com.finalproject.apartmentguide.ui.user.UserPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    @Provides
    @Singleton
    LoginMvpPresenter<LoginMvpView> provideLoginMvpPresenter(LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    SignUpMvpPresenter<SignUpMvpView> provideSignUpMvpPresenter(SignUpPresenter<SignUpMvpView> presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    AdvertisementMvpPresenter<AdvertisementMvpView> provideAdvertisementPresenter(AdvertisementPresenter<AdvertisementMvpView> advertisementPresenter) {
        return advertisementPresenter;
    }

    @Provides
    @Singleton
    AdvertisementDetailsMvpPresenter<AdvertisementDetailsMvpView> provideAdvertisementDetailsPresenter(ApartmentDetailsPresenter<AdvertisementDetailsMvpView> presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    CommonMvpPresenter<CommonMvpView> provideCommonMvpPresenter(CommonPresenter<CommonMvpView> presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    CreateAdMvpPresenter<CreateAdMvpView> provideCreateAdPresenter(CreateAdPresenter<CreateAdMvpView> presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    UserMvpPresenter<UserMvpView> provideUserMvpPresenter(UserPresenter<UserMvpView> presenter) {
        return presenter;
    }

}
