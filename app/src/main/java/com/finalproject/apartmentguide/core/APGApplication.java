package com.finalproject.apartmentguide.core;

import android.app.Application;

import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.config.ApiEndPoints;
import com.finalproject.apartmentguide.core.dagger.component.ApplicationComponent;
import com.finalproject.apartmentguide.core.dagger.component.DaggerApplicationComponent;
import com.finalproject.apartmentguide.core.dagger.module.ActivityModule;
import com.finalproject.apartmentguide.core.dagger.module.ApplicationModule;
import com.finalproject.apartmentguide.core.dagger.module.CommonModule;
import com.finalproject.apartmentguide.core.dagger.module.NetworkModule;
import com.finalproject.apartmentguide.core.dagger.module.RepositoryModule;
import com.finalproject.apartmentguide.core.dagger.module.ServiceModule;

import javax.inject.Inject;


public class APGApplication extends Application {

    private static APGApplication application = null;
    private ApplicationComponent applicationComponent;
    @Inject
    PreferenceHelper preferenceHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        getDefault().getApplicationComponent().inject(this);
    }

    public static APGApplication getDefault() {

        return application;
    }

    public ApplicationComponent getApplicationComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule())
                .activityModule(new ActivityModule())
                .networkModule(new NetworkModule(ApiEndPoints.BASE_URL))
                .repositoryModule(new RepositoryModule())
                .serviceModule(new ServiceModule())
                .commonModule(new CommonModule())
                .build();
        return applicationComponent;
    }

    public boolean isLogin() {
        return preferenceHelper.getUserInformation() != null;
    }
}