package com.finalproject.apartmentguide.core.dagger.module;

import com.finalproject.apartmentguide.data.Service.AdvertisementService;
import com.finalproject.apartmentguide.data.Service.ApartmentService;
import com.finalproject.apartmentguide.data.Service.AuthenticationService;
import com.finalproject.apartmentguide.data.Service.CommonService;
import com.finalproject.apartmentguide.data.Service.UserApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
  
@Module
public class ServiceModule {

    @Provides
    @Singleton
    public AuthenticationService provideAuthentication(Retrofit retrofit) {
        return retrofit.create(AuthenticationService.class);
    }

    @Provides
    @Singleton
    public UserApiService provideUserService(Retrofit retrofit) {
        return retrofit.create(UserApiService.class);
    }

    @Provides
    @Singleton
    public AdvertisementService provideAdvertisementService(Retrofit retrofit) {
        return retrofit.create(AdvertisementService.class);
    }

    @Provides
    @Singleton
    public CommonService proCommonService(Retrofit retrofit) {
        return retrofit.create(CommonService.class);
    }

    @Provides
    @Singleton
    public ApartmentService provideAptService(Retrofit retrofit) {
        return retrofit.create(ApartmentService.class);
    }

}
