package com.finalproject.apartmentguide.model;

import com.google.gson.annotations.SerializedName;

public enum AvailableFor {
    @SerializedName("1")
    Bachelor(1),
    @SerializedName("2")
    Family(2),
    @SerializedName("3")
    Office(3),
    @SerializedName("0")
    Both(0);
    public int val;

    AvailableFor(int i) {
        this.val = i;
    }
}
