package com.finalproject.apartmentguide.model;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

public class Apartment {
    private int appartmentId;
    private String  title;
    private int locationId;
    private int userId;
    private String description;
    private Timestamp createDate;
    private Price price;
    private Advertisement advertisement;
    private ApartmentDetails apartmentDetails;
    private List<Attachments> attachments;
    private List<Review> reviews;
    private Location location;
    private User user;

    public int getAppartmentId() {
        return appartmentId;
    }

    public void setAppartmentId(int appartmentId) {
        this.appartmentId = appartmentId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int loationId) {
        this.locationId = loationId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Apartment apartment = (Apartment) o;

        if (appartmentId != apartment.appartmentId) return false;
        if (locationId != apartment.locationId) return false;
        if (userId != apartment.userId) return false;
        if (description != null ? !description.equals(apartment.description) : apartment.description != null)
            return false;
        if (createDate != null ? !createDate.equals(apartment.createDate) : apartment.createDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = appartmentId;
        result = 31 * result + locationId;
        result = 31 * result + userId;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        return result;
    }

    public Advertisement getAdvertisement() {
        return advertisement;
    }

    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }
    public ApartmentDetails getApartmentDetails() {
        return apartmentDetails;
    }

    public void setApartmentDetails(ApartmentDetails apartmentDetails) {
        this.apartmentDetails = apartmentDetails;
    }

    public List<Attachments> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachments> attachments) {
        this.attachments = attachments;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
