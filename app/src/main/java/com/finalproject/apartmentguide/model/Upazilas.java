package com.finalproject.apartmentguide.model;

import com.google.gson.annotations.SerializedName;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Upazilas {
    private int upozilaId;
    @SerializedName("district_id")
    private int districtId;
    private String name;
    private String bnName;
    private Districts districts;
    private List<Location> locations;

    public int getUpozilaId() {
        return upozilaId;
    }

    public void setUpozilaId(int upozilaId) {
        this.upozilaId = upozilaId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBnName() {
        return bnName;
    }

    public void setBnName(String bnName) {
        this.bnName = bnName;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Upazilas upazilas = (Upazilas) o;
        return upozilaId == upazilas.upozilaId &&
                districtId == upazilas.districtId &&
                Objects.equals(name, upazilas.name) &&
                Objects.equals(bnName, upazilas.bnName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(upozilaId, districtId, name, bnName);
    }

    public Districts getDistricts() {
        return districts;
    }

    public void setDistricts(Districts districts) {
        this.districts = districts;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}
