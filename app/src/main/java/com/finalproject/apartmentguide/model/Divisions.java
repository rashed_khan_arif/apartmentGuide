package com.finalproject.apartmentguide.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Divisions {
    @SerializedName("division_id")
    private int divisionId;
    private String name;
    private String bnName;
    private List<Districts> districts;

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBnName() {
        return bnName;
    }

    public void setBnName(String bnName) {
        this.bnName = bnName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Divisions divisions = (Divisions) o;
        return divisionId == divisions.divisionId &&
                Objects.equals(name, divisions.name) &&
                Objects.equals(bnName, divisions.bnName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(divisionId, name, bnName);
    }

    public List<Districts> getDistricts() {
        return districts;
    }

    public void setDistricts(List<Districts> districts) {
        this.districts = districts;
    }
}
