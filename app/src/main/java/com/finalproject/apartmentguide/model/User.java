package com.finalproject.apartmentguide.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

public class User {
    private int userId;
    private String fullName;
    private Date dob;
    private int userRoleId;
    private int upozilaId;
    private String cellNumber;
    private String address;
    private int zipCode;
    private String city;
    private String email;
    private String password;
    private String profileImage;
    private UserRole userRole;
    private List<Apartment> apartments;
    private int totalApartments;
    private int isVerified;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }


    public int getUpozilaId() {
        return upozilaId;
    }

    public void setUpozilaId(int upozilaId) {
        this.upozilaId = upozilaId;
    }

    public String getCellNumber() {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber) {
        this.cellNumber = cellNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (userId != user.userId) return false;
        if (userRoleId != user.userRoleId) return false;
        if (upozilaId != user.upozilaId) return false;
        if (cellNumber != user.cellNumber) return false;
        if (zipCode != user.zipCode) return false;
        if (fullName != null ? !fullName.equals(user.fullName) : user.fullName != null)
            return false;
        if (dob != null ? !dob.equals(user.dob) : user.dob != null) return false;
        if (address != null ? !address.equals(user.address) : user.address != null) return false;
        if (city != null ? !city.equals(user.city) : user.city != null) return false;
        if (profileImage != null ? !profileImage.equals(user.profileImage) : user.profileImage != null)
            return false;


        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
        result = 31 * result + (dob != null ? dob.hashCode() : 0);
        result = 31 * result + userRoleId;
        result = 31 * result + upozilaId;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + zipCode;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (profileImage != null ? profileImage.hashCode() : 0);
        return result;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Apartment> getApartments() {
        return apartments;
    }

    public void setApartments(List<Apartment> apartments) {
        this.apartments = apartments;
    }

    public int getTotalApartments() {
        return totalApartments;
    }

    public void setTotalApartments(int totalApartments) {
        this.totalApartments = totalApartments;
    }

    public boolean getIsVerified() {
        return isVerified==1;
    }

    public void setIsVerified(int isVerified) {
        this.isVerified = isVerified;
    }
}
