package com.finalproject.apartmentguide.model;

import java.sql.Timestamp;


public class Review {
    private int reviewId;
    private int reviewedUserId;
    private String comments;
    private int apartmentId;
    private Timestamp reviewDate;
    private Apartment apartment;
    private float rating;
    private User reviewedByUser;

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public int getReviewedUserId() {
        return reviewedUserId;
    }

    public void setReviewedUserId(int reviewedUserId) {
        this.reviewedUserId = reviewedUserId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(int apartmentId) {
        this.apartmentId = apartmentId;
    }

    public Timestamp getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(Timestamp reviewDate) {
        this.reviewDate = reviewDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Review review = (Review) o;

        if (reviewId != review.reviewId) return false;
        if (reviewedUserId != review.reviewedUserId) return false;
        if (apartmentId != review.apartmentId) return false;
        if (comments != null ? !comments.equals(review.comments) : review.comments != null)
            return false;
        if (reviewDate != null ? !reviewDate.equals(review.reviewDate) : review.reviewDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = reviewId;
        result = 31 * result + reviewedUserId;
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + apartmentId;
        result = 31 * result + (reviewDate != null ? reviewDate.hashCode() : 0);
        return result;
    }

    public Apartment getApartment() {
        return apartment;
    }

    public void setApartment(Apartment apartment) {
        this.apartment = apartment;
    }

    public User getReviewedByUser() {
        return reviewedByUser;
    }

    public void setReviewedByUser(User reviewedByUser) {
        this.reviewedByUser = reviewedByUser;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
