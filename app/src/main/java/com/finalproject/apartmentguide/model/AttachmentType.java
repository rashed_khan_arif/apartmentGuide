package com.finalproject.apartmentguide.model;

import com.google.gson.annotations.SerializedName;

public enum AttachmentType {
    @SerializedName("1")
    Photo(1),
    @SerializedName("2")
    Video(2);
    public int val;

    AttachmentType(int i) {
        this.val = i;
    }
}
