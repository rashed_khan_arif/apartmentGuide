package com.finalproject.apartmentguide.model;

import com.google.gson.annotations.SerializedName;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

public class Districts {
    @SerializedName("district_id")
    private int districtId;
    @SerializedName("division_id")
    private int divisionId;
    private String name;
    private String bnName;
    private Double lat;
    private Double lon;
    private String website;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private List<Upazilas> upazilas;
    private Divisions divisions;

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBnName() {
        return bnName;
    }

    public void setBnName(String bnName) {
        this.bnName = bnName;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Districts districts = (Districts) o;
        return districtId == districts.districtId &&
                divisionId == districts.divisionId &&
                Objects.equals(name, districts.name) &&
                Objects.equals(bnName, districts.bnName) &&
                Objects.equals(lat, districts.lat) &&
                Objects.equals(lon, districts.lon) &&
                Objects.equals(website, districts.website) &&
                Objects.equals(createdAt, districts.createdAt) &&
                Objects.equals(updatedAt, districts.updatedAt);
    }

    @Override
    public int hashCode() {

        return Objects.hash(districtId, divisionId, name, bnName, lat, lon, website, createdAt, updatedAt);
    }

    public List<Upazilas> getUpazilas() {
        return upazilas;
    }

    public void setUpazilas(List<Upazilas> upazilas) {
        this.upazilas = upazilas;
    }

    public Divisions getDivisions() {
        return divisions;
    }

    public void setDivisions(Divisions divisions) {
        this.divisions = divisions;
    }
}
