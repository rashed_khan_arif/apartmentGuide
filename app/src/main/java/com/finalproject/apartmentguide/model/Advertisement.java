package com.finalproject.apartmentguide.model;

import java.sql.Timestamp;
 
public class Advertisement {
        private int advertisementId;
        private String title;
        private int apartmentId;
        private int isPublished;
        private int upozilaId;
        private AdType adType;
        private AvailableFor availableFor;
        private Timestamp createdDate;
        private Timestamp publishedDate;
        private Apartment apartment;
        private int userId;

        public int getAdvertisementId() {
            return advertisementId;
        }

        public void setAdvertisementId(int advertisementId) {
            this.advertisementId = advertisementId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getApartmentId() {
            return apartmentId;
        }

        public void setApartmentId(int apartmentId) {
            this.apartmentId = apartmentId;
        }

        public int getIsPublished() {
            return isPublished;
        }

        public boolean published() {
            return isPublished == 1;
        }

        public void setIsPublished(int isPublished) {
            this.isPublished = isPublished;
        }
        public int getUpozilaId() {
            return upozilaId;
        }

        public void setUpozilaId(int upozilaId) {
            this.upozilaId = upozilaId;
        }

        public AdType getAdType() {
            return adType;
        }

        public void setAdType(AdType adType) {
            this.adType = adType;
        }

        public AvailableFor getAvailableFor() {
            return availableFor;
        }

        public void setAvailableFor(AvailableFor availableFor) {
            this.availableFor = availableFor;
        }

        public Timestamp getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Timestamp createdDate) {
            this.createdDate = createdDate;
        }

        public Timestamp getPublishedDate() {
            return publishedDate;
        }

        public void setPublishedDate(Timestamp publishedDate) {
            this.publishedDate = publishedDate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Advertisement that = (Advertisement) o;

            if (advertisementId != that.advertisementId) return false;
            if (apartmentId != that.apartmentId) return false;
            if (isPublished != that.isPublished) return false;
            if (upozilaId != that.upozilaId) return false;
            if (adType != that.adType) return false;
            if (availableFor != that.availableFor) return false;
            if (title != null ? !title.equals(that.title) : that.title != null) return false;
            if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null)
                return false;
            return publishedDate != null ? publishedDate.equals(that.publishedDate) : that.publishedDate == null;
        }

        @Override
        public int hashCode() {
            int result = advertisementId;
            result = 31 * result + (title != null ? title.hashCode() : 0);
            result = 31 * result + apartmentId;
            result = 31 * result + isPublished;
            result = 31 * result + upozilaId;
            result = 31 * result + adType.val;
            result = 31 * result + availableFor.val;
            result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
            result = 31 * result + (publishedDate != null ? publishedDate.hashCode() : 0);
            return result;
        }


        public Apartment getApartment() {
            return apartment;
        }

        public void setApartment(Apartment apartment) {
            this.apartment = apartment;
        }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
