package com.finalproject.apartmentguide.model;

 

public enum UserType {
    SELLER(2), CUSTOMER(3);
    public int val;

    UserType(int i) {
        this.val = i;
    }
}
