package com.finalproject.apartmentguide.model;

public class PriceDetails {
    private int priceDetailsId;
    private int priceId;
    private double electricityCharge;
    private double liftCharge;
    private double waterCharge;
    private double gaurdCharge;
    private double othersCharge;

    private Price price;

    public int getPriceDetailsId() {
        return priceDetailsId;
    }

    public void setPriceDetailsId(int priceDetailsId) {
        this.priceDetailsId = priceDetailsId;
    }

    public int getPriceId() {
        return priceId;
    }

    public void setPriceId(int priceId) {
        this.priceId = priceId;
    }

    public double getElectricityCharge() {
        return electricityCharge;
    }

    public void setElectricityCharge(double electricityCharge) {
        this.electricityCharge = electricityCharge;
    }

    public double getLiftCharge() {
        return liftCharge;
    }

    public void setLiftCharge(double liftCharge) {
        this.liftCharge = liftCharge;
    }

    public double getWaterCharge() {
        return waterCharge;
    }

    public void setWaterCharge(double waterCharge) {
        this.waterCharge = waterCharge;
    }

    public double getGaurdCharge() {
        return gaurdCharge;
    }

    public void setGaurdCharge(double gaurdCharge) {
        this.gaurdCharge = gaurdCharge;
    }

    public double getOthersCharge() {
        return othersCharge;
    }

    public void setOthersCharge(double othersCharge) {
        this.othersCharge = othersCharge;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PriceDetails that = (PriceDetails) o;

        if (priceDetailsId != that.priceDetailsId) return false;
        if (priceId != that.priceId) return false;
        if (Double.compare(that.electricityCharge, electricityCharge) != 0) return false;
        if (Double.compare(that.liftCharge, liftCharge) != 0) return false;
        if (Double.compare(that.waterCharge, waterCharge) != 0) return false;
        if (Double.compare(that.gaurdCharge, gaurdCharge) != 0) return false;
        if (Double.compare(that.othersCharge, othersCharge) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = priceDetailsId;
        result = 31 * result + priceId;
        temp = Double.doubleToLongBits(electricityCharge);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(liftCharge);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(waterCharge);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(gaurdCharge);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(othersCharge);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }
}
