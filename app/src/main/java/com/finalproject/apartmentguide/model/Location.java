package com.finalproject.apartmentguide.model;

import java.util.List;
import java.util.Objects;

public class Location {
    private int locationId;
    private int upozilaId;
    private String locationName;
    private String streetAddress;
    private String detailsAddress;
    private double lat;
    private double lng;
    private List<Apartment> apartment;
    private Upazilas upazilas;

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public int getUpozilaId() {
        return upozilaId;
    }

    public void setUpozilaId(int upozilaId) {
        this.upozilaId = upozilaId;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getDetailsAddress() {
        return detailsAddress;
    }

    public void setDetailsAddress(String detailsAddress) {
        this.detailsAddress = detailsAddress;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return locationId == location.locationId &&
                upozilaId == location.upozilaId &&
                Double.compare(location.lat, lat) == 0 &&
                Objects.equals(streetAddress, location.streetAddress) &&
                Objects.equals(detailsAddress, location.detailsAddress);
    }

    @Override
    public int hashCode() {

        return Objects.hash(locationId, upozilaId, streetAddress, detailsAddress, lat);
    }

    public List<Apartment> getApartment() {
        return apartment;
    }

    public void setApartment(List<Apartment> apartment) {
        this.apartment = apartment;
    }

    public Upazilas getUpazilas() {
        return upazilas;
    }

    public void setUpazilas(Upazilas upazilas) {
        this.upazilas = upazilas;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
