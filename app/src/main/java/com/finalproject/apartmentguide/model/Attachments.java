package com.finalproject.apartmentguide.model;


import android.net.Uri;

import java.sql.Timestamp;

public class Attachments {
    private int attachmentId;
    private AttachmentType attachType;
    private String fileName;
    private int apartmentId;
    private boolean isTitle;
    private Apartment apartment;
    private transient Uri filePath;


    public int getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(int attachmentId) {
        this.attachmentId = attachmentId;
    }

    public AttachmentType getAttachType() {
        return attachType;
    }

    public void setAttachType(AttachmentType attachType) {
        this.attachType = attachType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(int apartmentId) {
        this.apartmentId = apartmentId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Attachments that = (Attachments) o;

        if (attachmentId != that.attachmentId) return false;
        if (attachType != that.attachType) return false;
        if (apartmentId != that.apartmentId) return false;
        if (fileName != null ? !fileName.equals(that.fileName) : that.fileName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = attachmentId;
        result = 31 * result + attachType.val;
        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
        result = 31 * result + apartmentId;
        return result;
    }

    public Apartment getApartment() {
        return apartment;
    }

    public void setApartment(Apartment apartment) {
        this.apartment = apartment;
    }

    public boolean isTitle() {
        return isTitle;
    }

    public void setTitle(boolean title) {
        isTitle = title;
    }

    public Uri getFilePath() {
        return filePath;
    }

    public void setFilePath(Uri filePath) {
        this.filePath = filePath;
    }
}
