package com.finalproject.apartmentguide.model;

public class ApartmentDetails {
    private int apartmentDetailsId;
    private String size;
    private int room;
    private int kitchen;
    private int bath;
    private int corridor;
    private String color;
    private int apartmentId;
    private Apartment apartment;

    public int getApartmentDetailsId() {
        return apartmentDetailsId;
    }

    public void setApartmentDetailsId(int apartmentDetailsId) {
        this.apartmentDetailsId = apartmentDetailsId;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public int getKitchen() {
        return kitchen;
    }

    public void setKitchen(int kitchen) {
        this.kitchen = kitchen;
    }

    public int getBath() {
        return bath;
    }

    public void setBath(int bath) {
        this.bath = bath;
    }

    public int getCorridor() {
        return corridor;
    }

    public void setCorridor(int corridor) {
        this.corridor = corridor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(int apartmentId) {
        this.apartmentId = apartmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApartmentDetails that = (ApartmentDetails) o;

        if (apartmentDetailsId != that.apartmentDetailsId) return false;
        if (room != that.room) return false;
        if (kitchen != that.kitchen) return false;
        if (bath != that.bath) return false;
        if (corridor != that.corridor) return false;
        if (apartmentId != that.apartmentId) return false;
        if (size != null ? !size.equals(that.size) : that.size != null) return false;
        if (color != null ? !color.equals(that.color) : that.color != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = apartmentDetailsId;
        result = 31 * result + (size != null ? size.hashCode() : 0);
        result = 31 * result + room;
        result = 31 * result + kitchen;
        result = 31 * result + bath;
        result = 31 * result + corridor;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + apartmentId;
        return result;
    }

    public Apartment getApartment() {
        return apartment;
    }

    public void setApartment(Apartment apartment) {
        this.apartment = apartment;
    }
}
