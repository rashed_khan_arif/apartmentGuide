package com.finalproject.apartmentguide.model;

import com.google.gson.annotations.SerializedName;

public enum AdType {
    @SerializedName("2")
    RENT(2),
    @SerializedName("1")
    SELL(1);

    public int val;

    AdType(int i) {
        this.val = i;
    }

    public static AdType getFromInt(int i) {
        if (i == 1) {
            return SELL;
        }
        if (i == 2) {
            return RENT;
        }
        return null;
    }
}
