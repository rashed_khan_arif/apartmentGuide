package com.finalproject.apartmentguide.data.repository.impl;

import com.finalproject.apartmentguide.data.Service.ApartmentService;
import com.finalproject.apartmentguide.data.repository.ApartmentRepository;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.Attachments;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class ApartmentRepositoryImpl implements ApartmentRepository {
    ApartmentService service;

    @Inject
    public ApartmentRepositoryImpl(ApartmentService service) {
        this.service = service;
    }


    @Override
    public Observable<Apartment> getApartmentById(int id) {
        return service.getapartmentById(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Apartment>> getApartments() {
        return null;
    }

    @Override
    public Observable<List<Apartment>> getApartmentsByUserId(int userId) {
        return service.getapartmentByUserId(userId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Object> postApartment(Apartment apartment) {
        return service.addapartment(apartment).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Object> postImages(String images) {
        return service.addImages(images).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Object> postImages(MultipartBody.Part part, RequestBody name) {
        return service.postImage(part,name).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
