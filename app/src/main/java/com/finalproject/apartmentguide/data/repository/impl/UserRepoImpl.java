package com.finalproject.apartmentguide.data.repository.impl;

import android.content.Context;

import com.finalproject.apartmentguide.data.Service.UserApiService;
import com.finalproject.apartmentguide.data.repository.UserRepository;
import com.finalproject.apartmentguide.model.Credentials;
import com.finalproject.apartmentguide.model.LoginResult;
import com.finalproject.apartmentguide.model.User;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class UserRepoImpl implements UserRepository {

    UserApiService apiService;
    Context context;

    @Inject
    public UserRepoImpl(UserApiService apiService, Context context) {
        this.apiService = apiService;
        this.context = context;
    }

    @Override
    public Observable<LoginResult> registerUser(Credentials user) {
        return apiService.registerUser(user).subscribeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<User> getUserDetails(int userId) {
        return apiService.getUserDetails(userId).subscribeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<User> updateUser(User user) {
        return apiService.updateUser(user).subscribeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread());
    }
}
