package com.finalproject.apartmentguide.data.Service;

import com.finalproject.apartmentguide.config.ApiEndPoints;
import com.finalproject.apartmentguide.model.Districts;
import com.finalproject.apartmentguide.model.Divisions;
import com.finalproject.apartmentguide.model.Location;
import com.finalproject.apartmentguide.model.Upazilas;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

 

public interface CommonService {
    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "divisions")
    Observable<List<Divisions>> getDivisions();

    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "districts?")
    Observable<List<Districts>> getDistricts(@Query("divisionId") int divisionId);

    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "upozilas?")
    Observable<List<Upazilas>> getUpazilas(@Query("districtId") int districtId);

    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "locations?")
    Observable<List<Location>> getLocations(@Query("upzId") int upzId);
}
