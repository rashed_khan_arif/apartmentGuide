package com.finalproject.apartmentguide.data.repository;

import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.Attachments;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public interface ApartmentRepository {
    Observable<Apartment> getApartmentById(int id);

    Observable<List<Apartment>> getApartments();

    Observable<List<Apartment>> getApartmentsByUserId(int userId);

    Observable<Object> postApartment(Apartment apartment);

    Observable<Object> postImages(String images);

    Observable<Object> postImages(MultipartBody.Part part, RequestBody name);
}
