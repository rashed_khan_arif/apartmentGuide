package com.finalproject.apartmentguide.data.repository.impl;

import android.content.Context;

import com.finalproject.apartmentguide.data.Service.CommonService;
import com.finalproject.apartmentguide.data.repository.CommonRepository;
import com.finalproject.apartmentguide.model.Districts;
import com.finalproject.apartmentguide.model.Divisions;
import com.finalproject.apartmentguide.model.Location;
import com.finalproject.apartmentguide.model.Upazilas;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

 
public class CommonRepositoryImpl implements CommonRepository {
    CommonService service;
    Context context;

    @Inject
    public CommonRepositoryImpl(CommonService commonService, Context context) {
        this.service = commonService;
        this.context = context;
    }

    @Override
    public Observable<List<Divisions>> getDivisions() {
        return service.getDivisions().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Districts>> getDistricts(int divId) {
        return service.getDistricts(divId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Upazilas>> getUpazilas(int disId) {
        return service.getUpazilas(disId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Location>> getLocations(int upzId) {
        return service.getLocations(upzId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
