package com.finalproject.apartmentguide.data.repository;

import com.finalproject.apartmentguide.model.AdType;
import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.Review;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface AdvertisementRepository {

    Observable<List<Advertisement>> getAdvertisements(int divId, int disId, int upzId, AdType adType, boolean top);

    Observable<String> addAdvertisement(Advertisement ad);

    Observable<Advertisement> getAdvertisement(int id);

    Observable<List<Advertisement>> getAdvertisementByUserID(int id);

    Observable<List<Apartment>> getApartmentsByUserID(int id);

    Observable<Object> submitRateAndReview(Review review);

    Observable<List<Review>> getReviewsByAptId(int aptId);

    Observable<List<Review>> getReviewsByUserId(int userId);

    Observable<Object> updateStatus(int adId, int status);

    ;
}
