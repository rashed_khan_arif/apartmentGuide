package com.finalproject.apartmentguide.data.repository.impl;

import android.content.Context;

import com.finalproject.apartmentguide.data.Service.AuthenticationService;
import com.finalproject.apartmentguide.data.repository.AuthenticationRepo;
import com.finalproject.apartmentguide.model.Credentials;
import com.finalproject.apartmentguide.model.LoginResult;
import com.finalproject.apartmentguide.model.Verify;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class AuthenticationRepoImpl implements AuthenticationRepo {
    private AuthenticationService authenticationService;
    private Context context;

    @Inject
    public AuthenticationRepoImpl(AuthenticationService authenticationService, Context context) {
        this.authenticationService = authenticationService;
        this.context = context;
    }

    @Override
    public Observable<LoginResult> doLogin(Credentials loginResult) {
        return authenticationService.authentication(loginResult).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Object> sendCode(int id) {
        return authenticationService.sendCode(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Object> checkCode(Verify body) {
        return authenticationService.checkCode(body).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
