package com.finalproject.apartmentguide.data.Service;

import com.finalproject.apartmentguide.config.ApiEndPoints;
import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Credentials;
import com.finalproject.apartmentguide.model.LoginResult;
import com.finalproject.apartmentguide.model.User;
import com.google.android.gms.common.api.Api;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface UserApiService {
    @POST(ApiEndPoints.BASE_URL + "Account/register")
    Observable<LoginResult> registerUser(@Body Credentials user);

    @POST(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "users/add-or-update")
    Observable<User> updateUser(@Body User user);

    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "user-by-id?")
    Observable<User> getUserDetails(@Query("userId") int id);

}
