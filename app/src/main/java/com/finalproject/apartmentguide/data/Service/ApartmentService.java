package com.finalproject.apartmentguide.data.Service;

import com.finalproject.apartmentguide.config.ApiEndPoints;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.Attachments;


import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApartmentService {
    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "apartments")
    Observable<Apartment> getapartments();

    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "apartments-by-id?")
    Observable<Apartment> getapartmentById(@Query("id") int id);

    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "apartments-by-user-id?")
    Observable<List<Apartment>> getapartmentByUserId(@Query("userId") int userId);

    @POST(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "apartments-add")
    Observable<Object> addapartment(@Body Apartment body);

    @POST(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "upload-image")
    Observable<Object> addImages(@Body String body);


    @Multipart
    @POST(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "upload-image")
    Observable<Object> postImage(@Part MultipartBody.Part image, @Part("body") RequestBody body);


}
