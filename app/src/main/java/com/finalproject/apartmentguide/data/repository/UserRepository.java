package com.finalproject.apartmentguide.data.repository;

import com.finalproject.apartmentguide.model.Credentials;
import com.finalproject.apartmentguide.model.LoginResult;
import com.finalproject.apartmentguide.model.User;

import io.reactivex.Observable;


public interface UserRepository {
    Observable<LoginResult> registerUser(Credentials user);

    Observable<User> updateUser(User user);

    Observable<User> getUserDetails(int userId);
}
