package com.finalproject.apartmentguide.data.repository;

import com.finalproject.apartmentguide.model.Districts;
import com.finalproject.apartmentguide.model.Divisions;
import com.finalproject.apartmentguide.model.Location;
import com.finalproject.apartmentguide.model.Upazilas;

import java.util.List;

import io.reactivex.Observable;

 

public interface CommonRepository {
    Observable<List<Divisions>> getDivisions();

    Observable<List<Districts>> getDistricts(int divId);

    Observable<List<Upazilas>> getUpazilas(int disId);

    Observable<List<Location>> getLocations(int upzId);
}
