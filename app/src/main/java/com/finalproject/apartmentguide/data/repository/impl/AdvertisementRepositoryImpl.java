package com.finalproject.apartmentguide.data.repository.impl;

import android.content.Context;

import com.finalproject.apartmentguide.data.Service.AdvertisementService;
import com.finalproject.apartmentguide.data.repository.AdvertisementRepository;
import com.finalproject.apartmentguide.model.AdType;
import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.Review;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class AdvertisementRepositoryImpl implements AdvertisementRepository {
    AdvertisementService advertisementService;
    Context context;

    @Inject
    public AdvertisementRepositoryImpl(AdvertisementService advertisementService, Context context) {
        this.advertisementService = advertisementService;
        this.context = context;
    }

    @Override
    public Observable<List<Advertisement>> getAdvertisements(int divId, int disId, int upzId, AdType adType, boolean top) {
        return advertisementService.getAdvertisementsList(divId, disId, upzId, adType.val, top).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<String> addAdvertisement(Advertisement ad) {
        return advertisementService.addAdvertisement(ad).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Advertisement> getAdvertisement(int id) {
        return advertisementService.getAdvertisement(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Advertisement>> getAdvertisementByUserID(int id) {
        return advertisementService.getAdvertisementsListByUserId(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Apartment>> getApartmentsByUserID(int id) {
        return advertisementService.getApartmentsListByUserId(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Object> submitRateAndReview(Review review) {
        return advertisementService.addApartmentReview(review).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Review>> getReviewsByAptId(int aptId) {
        return advertisementService.getReviewsByAptId(aptId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

    }

    @Override
    public Observable<List<Review>> getReviewsByUserId(int userId) {
        return advertisementService.getReviewListByUserId(userId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

    }

    @Override
    public Observable<Object> updateStatus(int adId, int status) {
        return advertisementService.updateStatus(adId, status).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
