package com.finalproject.apartmentguide.data.repository;

import com.finalproject.apartmentguide.model.Credentials;
import com.finalproject.apartmentguide.model.LoginResult;
import com.finalproject.apartmentguide.model.Verify;

import io.reactivex.Observable;


public interface AuthenticationRepo {
    Observable<LoginResult> doLogin(Credentials credentials);

    Observable<Object> sendCode(int id);

    Observable<Object> checkCode(Verify body);

}
