package com.finalproject.apartmentguide.data.Service;

import com.finalproject.apartmentguide.config.ApiEndPoints;
import com.finalproject.apartmentguide.data.ApiService;
import com.finalproject.apartmentguide.model.Credentials;
import com.finalproject.apartmentguide.model.LoginResult;
import com.finalproject.apartmentguide.model.User;
import com.finalproject.apartmentguide.model.Verify;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface AuthenticationService {

    @POST(ApiEndPoints.API_VERSION + "authentication")
    Observable<LoginResult> authentication(@Body Credentials loginResult);

    @GET(ApiEndPoints.API_VERSION + "send-code")
    Observable<Object> sendCode(@Query("userId") int userId);

    @POST(ApiEndPoints.API_VERSION + "check-code")
    Observable<Object> checkCode(@Body Verify body);

}
