package com.finalproject.apartmentguide.data.Service;

import com.finalproject.apartmentguide.config.ApiEndPoints;
import com.finalproject.apartmentguide.model.AdType;
import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.Review;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface AdvertisementService {
    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "advertisements-by-id?")
    Observable<Advertisement> getAdvertisement(@Query("id") int id);

    @POST(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "advertisements-add")
    Observable<String> addAdvertisement(@Body Advertisement body);

    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "advertisements?")
    Observable<List<Advertisement>> getAdvertisementsList(@Query("divId") int divId, @Query("disId") int disId, @Query("upzId") int upzId, @Query("adType") int adType, @Query("top") boolean top);

    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "advertisements-by-user?")
    Observable<List<Advertisement>> getAdvertisementsListByUserId(@Query("userId") int userId);

    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "apartments-by-user-id?")
    Observable<List<Apartment>> getApartmentsListByUserId(@Query("userId") int userId);

    @POST(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "add-review")
    Observable<Object> addApartmentReview(@Body Review body);

    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "reviews-by-user-id?")
    Observable<List<Review>> getReviewListByUserId(@Query("userId") int userId);

    @GET(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "reviews-by-apt-id?")
    Observable<List<Review>> getReviewsByAptId(@Query("apartmentId") int apartmentId);

    @FormUrlEncoded
    @POST(ApiEndPoints.BASE_URL + ApiEndPoints.API_VERSION + "update-status")
    Observable<Object> updateStatus(@Field("adId") int adId, @Field("status") int status);

}
