package com.finalproject.apartmentguide.config;


public class ApiEndPoints {

//    public static final String BASE_URL = "http://apartmentguidebd.com/";//production
    public static final String BASE_URL = "http://192.168.0.20/apartment/apt_guide_api/";
    public static final String API_VERSION = "api/v1/";
    public static String imageUrl = "/image";
    public static String loginUrl = "/authentication";
    public static String registrationUrl = "/Account/register";

    public static String getApartmentImageUrl(String image) {
        return BASE_URL + "image/" + image;
    }
}
