package com.finalproject.apartmentguide.config;

 

public class Constant {
    public static final String PREF_FILE = "APARTMENT_PREFERENCE_NAME";
    public static final String INTENT_INT_EXTRA = "intent_int_extra";
    public static final String INTENT_CHOOSE_OPTION = "intent_choose";
}
