package com.finalproject.apartmentguide.ui.account;

import android.annotation.SuppressLint;

import com.finalproject.apartmentguide.common.PreferenceManager;
import com.finalproject.apartmentguide.data.repository.AuthenticationRepo;
import com.finalproject.apartmentguide.model.Credentials;
import com.finalproject.apartmentguide.model.LoginResult;
import com.finalproject.apartmentguide.model.Verify;
import com.finalproject.apartmentguide.ui.base.BasePresenter;
import com.finalproject.apartmentguide.ui.base.MvpView;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;


public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V> implements LoginMvpPresenter<V> {
    private AuthenticationRepo authenticationRepo;
    private PreferenceManager preferenceManager;

    @Inject
    public LoginPresenter(AuthenticationRepo authenticationRepo, PreferenceManager prefManager) {
        this.authenticationRepo = authenticationRepo;
        this.preferenceManager = prefManager;
    }


    @SuppressLint("CheckResult")
    @Override
    public void doLogin(Credentials credentials) {
        getMvpView().showLoading();
        authenticationRepo.doLogin(credentials).subscribe(loginResult -> {
                preferenceManager.setUserInformation(loginResult.getUser());
        }, throwable -> {
            getMvpView().hideLoading();
            getMvpView().onError(throwable.getMessage());
        }, () -> {
            getMvpView().showMessage("Login Successful");
            getMvpView().hideLoading();
            getMvpView().loginSuccess();
        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void sendVerificationCode(int id) {
        getMvpView().showLoading();
        authenticationRepo.sendCode(id).subscribe(response -> {
            getMvpView().hideLoading();
            getMvpView().msgSent(response);
        }, throwable -> {
            getMvpView().hideLoading();
            getMvpView().onError(throwable.getMessage());
        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void checkVerificationCode(Verify body) {
        getMvpView().showLoading();
        authenticationRepo.checkCode(body).subscribe(response -> {
            getMvpView().hideLoading();
            getMvpView().userVerified();
        }, throwable -> {
            getMvpView().hideLoading();
            getMvpView().userVerificationFailed("Failed to verify !");
        });
    }


}
