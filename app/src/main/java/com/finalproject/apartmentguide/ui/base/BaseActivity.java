package com.finalproject.apartmentguide.ui.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Spinner;
import android.widget.Toast;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.common.SpinnerAdapter;
import com.finalproject.apartmentguide.common.Util;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.core.dagger.component.ActivityComponent;
import com.finalproject.apartmentguide.core.dagger.module.ActivityModule;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class BaseActivity extends AppCompatActivity implements MvpView {
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void showLoading() {
        hideLoading();
        progressDialog = Util.showLoadingDialog(this);
    }

    @Override
    public void hideLoading() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    @Override
    public void openActivityOnTokenExpire() {

    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {
        showMessage(message);
    }

    @Override
    public void showMessage(String message) {
        runOnUiThread(() -> Toast.makeText(this, message, Toast.LENGTH_LONG).show());

    }

    @Override
    public void showMessage(int resId) {

    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }

    @Override
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void generateSpinner(Spinner spinner, LinkedHashMap<Integer, String> data) {
        SpinnerAdapter<Integer, String> adapter = new SpinnerAdapter<Integer, String>(this, R.layout.spinner_item, R.id.tvSpinnerItemText, data);
        spinner.setAdapter(adapter);
    }

    public static int getIndex(Spinner spinner, int itemKey) {

        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            spinner.setSelection(i);
            Map.Entry<String, String> item = (Map.Entry<String, String>) spinner.getSelectedItem();
            int key;
            try {
                key = Integer.parseInt(item.getKey());
            } catch (ClassCastException e) {
                key = Integer.parseInt(String.valueOf(item.getKey()));
            }

            if (key == itemKey) {
                index = i;
            }
        }
        return index;
    }
}
