
package com.finalproject.apartmentguide.ui.account;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.common.Util;
import com.finalproject.apartmentguide.config.ApiEndPoints;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.model.User;
import com.finalproject.apartmentguide.model.Verify;
import com.finalproject.apartmentguide.ui.base.BaseActivity;
import com.finalproject.apartmentguide.ui.home.HomeActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Timer;

import javax.inject.Inject;

public class VerificationActivity extends BaseActivity implements LoginMvpView {
    private Button btnSendCode, btnVerify;
    private EditText etUserVerifiedCode;
    private TextView tvUserPhoneNumber, tvResendCode;
    private LinearLayout llVerifyLayout;
    private RelativeLayout llVerifiedCode;
    @Inject
    LoginMvpPresenter<LoginMvpView> presenter;
    @Inject
    PreferenceHelper prefHelper;
    @Inject
    Gson gson;
    private CountDownTimer countDownTimer;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        APGApplication.getDefault().getApplicationComponent().inject(this);
        initialize();
        bindComponents();
        setDataToView();
    }

    private void setDataToView() {
        if (prefHelper.getUserInformation() != null) {
            tvUserPhoneNumber.setText(String.valueOf(prefHelper.getUserInformation().getCellNumber()));
        }
    }

    @Override
    public void initialize() {
        presenter.onAttach(this);
        llVerifyLayout = findViewById(R.id.llVerifyLayout);
        llVerifiedCode = findViewById(R.id.llVerifiedCode);
        etUserVerifiedCode = findViewById(R.id.etUserVerifiedCode);
        tvResendCode = findViewById(R.id.tvResendCode);
        tvUserPhoneNumber = findViewById(R.id.tvUserPhoneNumber);
        btnSendCode = findViewById(R.id.btnSendCode);
        btnVerify = findViewById(R.id.btnVerify);
        btnSendCode.setOnClickListener(v -> {

            presenter.sendVerificationCode(prefHelper.getUserInformation().getUserId());
        });
        tvResendCode.setOnClickListener(v -> {
            etUserVerifiedCode.setEnabled(true);
            etUserVerifiedCode.setClickable(true);
            presenter.sendVerificationCode(prefHelper.getUserInformation().getUserId());
        });
        btnVerify.setOnClickListener(v -> {
            if (TextUtils.isEmpty(etUserVerifiedCode.getText())) {
                showMessage("Verification code is required !");
                return;
            }
            presenter.checkVerificationCode(new Verify(etUserVerifiedCode.getText().toString(), prefHelper.getUserInformation().getUserId()));
        });
    }

    @Override
    public void bindComponents() {

    }

    @Override
    public void signUpClick() {

    }

    @Override
    public void onLoginClick() {

    }

    @Override
    public void loginSuccess() {

    }

    @Override
    public void onBackPressed() {

    }

    private int time = 120;

    @Override
    public void msgSent(Object obj) {
        showMessage("Please check your inbox !");
        llVerifyLayout.setVisibility(View.GONE);
        llVerifiedCode.setVisibility(View.VISIBLE);
        etUserVerifiedCode.requestFocus();
        tvResendCode.setClickable(false);
        countDownTimer = new CountDownTimer(120000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                time--;
                tvResendCode.setText(calculateTime(time));
            }

            @Override
            public void onFinish() {
                tvResendCode.setClickable(true);
                tvResendCode.setText("Resend Code");
                etUserVerifiedCode.setEnabled(false);
                etUserVerifiedCode.setClickable(false);

            }
        };
        countDownTimer.start();
    }

    private String calculateTime(int time) {
        StringBuilder sb = new StringBuilder();
        int min = time / 60;
        sb.append(min).append(" : ");
        sb.append(time % 60);
        return sb.toString();
    }

    @Override
    public void userVerified() {
        showMessage("Verified");
        User user = prefHelper.getUserInformation();
        user.setIsVerified(1);
        prefHelper.setUserInformation(user);
        startActivity(new Intent(this, HomeActivity.class));
    }

    @Override
    public void userVerificationFailed(String msg) {
        showMessage(msg);
    }
}
