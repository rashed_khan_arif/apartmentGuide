package com.finalproject.apartmentguide.ui.account;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.model.Credentials;
import com.finalproject.apartmentguide.model.UserType;
import com.finalproject.apartmentguide.ui.advertisement.AdvertisementActivity;
import com.finalproject.apartmentguide.ui.base.BaseActivity;
import com.finalproject.apartmentguide.ui.home.ChooseOptActivity;
import com.finalproject.apartmentguide.ui.home.HomeActivity;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

public class SignUpActivity extends BaseActivity implements SignUpMvpView {
    private EditText etFullName, etEmail, etPassword, etConfirmPassword;
    private Button btnRegister;
    private Spinner spnUserType;

    @Inject
    SignUpMvpPresenter<SignUpMvpView> presenter;
    @Inject
    PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        APGApplication.getDefault().getApplicationComponent().inject(this);
        initialize();
        bindComponents();
        presenter.onAttach(this);
    }

    @Override
    public void initialize() {
        etFullName = findViewById(R.id.etFullName);
        etEmail = findViewById(R.id.etEmail);
        spnUserType = findViewById(R.id.spnUserType);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        btnRegister = findViewById(R.id.btnRegister);
        setSupportActionBar(findViewById(R.id.toolbar));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (APGApplication.getDefault().isLogin()) {
            onBackPressed();
        }
    }

    @Override
    public void bindComponents() {
        btnRegister.setOnClickListener(v -> onSignUpClick());
        LinkedHashMap<Integer, String> items = new LinkedHashMap<>();
        items.put(0, "--Select Type--");
        items.put(UserType.SELLER.val, UserType.SELLER.name());
        items.put(UserType.CUSTOMER.val, UserType.CUSTOMER.name());
        generateSpinner(spnUserType, items);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Account");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onSignUpClick() {
        if (isDataValid()) {
            Credentials user = new Credentials();
            user.setFullName(etFullName.getText().toString());
            user.setEmail(etEmail.getText().toString());
            user.setPassword(etPassword.getText().toString());
            user.setUserRoleId(getUserType());
            presenter.doSignUp(user);
        }
    }

    @Override
    public void signUpCompleted() {
        if (!preferenceHelper.getUserInformation().getIsVerified()) {
            startActivity(new Intent(this, VerificationActivity.class));
            return;
        }
        startActivity(new Intent(this, ChooseOptActivity.class));
    }

    private boolean isDataValid() {
        if (TextUtils.isEmpty(etFullName.getText())) {
            etFullName.setError("Full Name is required !");
            return false;
        }
        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError("Email is required !");
            return false;
        } else {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.getText()).matches()) {
                etEmail.setError("Invalid Email Address");
                return false;
            }
        }
        if (getUserType() == 0) {
            showMessage("User type is required !");
            return false;
        }

        if (TextUtils.isEmpty(etPassword.getText())) {
            etPassword.setError("Password is required !");
            return false;
        }
        if (TextUtils.isEmpty(etConfirmPassword.getText())) {
            etConfirmPassword.setError("Confirm Password is required !");
            return false;
        }
        if (!etPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
            showMessage("Password Doesn't Match !");
            return false;
        }

        return true;
    }

    private int getUserType() {
        Map.Entry<Integer, String> item = (Map.Entry<Integer, String>) spnUserType.getSelectedItem();
        return item.getKey();
    }
}
