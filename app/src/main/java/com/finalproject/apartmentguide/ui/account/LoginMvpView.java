package com.finalproject.apartmentguide.ui.account;

import com.finalproject.apartmentguide.ui.base.MvpView;


public interface LoginMvpView extends MvpView {

    void signUpClick();

    void onLoginClick();

    void loginSuccess();

    void msgSent(Object obj);

    void userVerified();

    void userVerificationFailed(String msg);
}
