package com.finalproject.apartmentguide.ui.account;

import com.finalproject.apartmentguide.ui.base.MvpView;

 
public interface SignUpMvpView extends MvpView {
    void onSignUpClick();

    void signUpCompleted();
}
