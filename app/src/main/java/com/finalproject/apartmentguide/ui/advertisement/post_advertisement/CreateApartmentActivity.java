package com.finalproject.apartmentguide.ui.advertisement.post_advertisement;

import android.Manifest;
import android.app.Dialog;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.config.ApiEndPoints;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.ApartmentDetails;
import com.finalproject.apartmentguide.model.AttachmentType;
import com.finalproject.apartmentguide.model.Attachments;
import com.finalproject.apartmentguide.model.Districts;
import com.finalproject.apartmentguide.model.Divisions;
import com.finalproject.apartmentguide.model.Location;
import com.finalproject.apartmentguide.model.Price;
import com.finalproject.apartmentguide.model.PriceDetails;
import com.finalproject.apartmentguide.model.Upazilas;
import com.finalproject.apartmentguide.ui.base.BaseActivity;
import com.finalproject.apartmentguide.ui.common.CommonMvpView;
import com.finalproject.apartmentguide.ui.common.CommonPresenter;
import com.google.gson.Gson;
import com.smarteist.autoimageslider.SliderView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CreateApartmentActivity extends BaseActivity implements CommonMvpView, CreateAdMvpView {
    private static final int READ_EXTERNAL_STORAGE = 111;
    private Spinner spnUpazila, spnDistrict, spnDivision;
    private EditText etAptTitle, etDescription, tvAmount, tvElectricityCharges, tvLiftCharge, tvWaterCharge, tvGuardCharge, tvOthersCharge,
            etLocationName, etStreetAddress, etDetailsAddress, etSize, etRoom, etBath, etKitchen, etColor, etCorridor;
    private LinearLayout llPrice, llApartment;

    private ImageButton ibtnSelectImage;
    private List<Attachments> attachments = new ArrayList<>();
    private Button btnSaveApartment;
    private boolean isImageSelectionMode;
    @Inject
    CreateAdMvpPresenter<CreateAdMvpView> presenter;
    @Inject
    PreferenceHelper preferenceHelper;
    @Inject
    CommonPresenter<CommonMvpView> commonPresenter;
    private int divId, disId, upzId;
    @Inject
    Gson gson;
    private int selectedApartmentId;
    private Apartment updateApt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_create_apartment);
        APGApplication.getDefault().getApplicationComponent().inject(this);
        initialize();
    }

    @Override
    public void initialize() {
        ibtnSelectImage = findViewById(R.id.ibtnSelectImage);
        etAptTitle = findViewById(R.id.etAptTitle);
        etAptTitle = findViewById(R.id.etAptTitle);
        etDescription = findViewById(R.id.etDescription);
        tvAmount = findViewById(R.id.etAmount);
        tvElectricityCharges = findViewById(R.id.etElectricityCharges);
        tvLiftCharge = findViewById(R.id.etLiftCharge);
        tvWaterCharge = findViewById(R.id.etWaterCharge);
        tvGuardCharge = findViewById(R.id.etGuardCharge);
        tvOthersCharge = findViewById(R.id.etOthersCharge);
        etLocationName = findViewById(R.id.etLocationName);
        etStreetAddress = findViewById(R.id.etStreetAddress);
        etDetailsAddress = findViewById(R.id.etDetailsAddress);
        etSize = findViewById(R.id.etSize);
        etRoom = findViewById(R.id.etRoom);
        etBath = findViewById(R.id.etBath);
        etKitchen = findViewById(R.id.etKitchen);
        etColor = findViewById(R.id.etColor);
        etCorridor = findViewById(R.id.etCorridor);
        //  spnSelectApartment = findViewById(R.id.spnSelectApartment);
        spnUpazila = findViewById(R.id.spnUpazila);
        spnDistrict = findViewById(R.id.spnDistrict);
        spnDivision = findViewById(R.id.spnDivision);
        llPrice = findViewById(R.id.llPrice);
        llApartment = findViewById(R.id.llApartment);
        btnSaveApartment = findViewById(R.id.btnSaveApartment);
        bindComponents();
    }

    @Override
    public void bindComponents() {
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Apartment");
        btnSaveApartment.setOnClickListener(v -> {
            saveApartment();
        });
        ibtnSelectImage.setOnClickListener(v -> {
            isImageSelectionMode = true;
            updateImage();
        });
        String apt = getIntent().getStringExtra("apt");
        if (apt != null) {
            Apartment apartment = gson.fromJson(apt, Apartment.class);
            updateApt = apartment;
            selectedApartmentId = apartment.getAppartmentId();
            setAptDataToView(apartment);
        } else {
            findViewById(R.id.tvUpdImage).setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onAttach(this);
        commonPresenter.onAttach(this);
        if (!isImageSelectionMode)
            commonPresenter.getDivisions();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    private void saveApartment() {

        if (!APGApplication.getDefault().isLogin()) {
            showMessage("Please login before post any apartment !");
            return;
        }
        if (upzId == 0) {
            showMessage("Please select your upazila !");
            return;
        }
        Apartment apartment = new Apartment();
        if (updateApt != null) {
            apartment.setAppartmentId(updateApt.getAppartmentId());
        }
        apartment.setTitle(etAptTitle.getText().toString());
        apartment.setDescription(etDescription.getText().toString());
        apartment.setUserId(preferenceHelper.getUserInformation().getUserId());
        if (!etLocationName.getText().toString().isEmpty() || !etDetailsAddress.getText().toString().isEmpty() || !etStreetAddress.getText().toString().isEmpty() || upzId != 0) {
            Location location = new Location();
            if (updateApt != null) {
                location.setLocationId(updateApt.getLocationId());
            }
            location.setLocationName(etLocationName.getText().toString());
            location.setDetailsAddress(etDetailsAddress.getText().toString());
            location.setStreetAddress(etStreetAddress.getText().toString());
            location.setUpozilaId(upzId);
            apartment.setLocation(location);
        }
        if (!etSize.getText().toString().isEmpty() || !etRoom.getText().toString().isEmpty() || !etBath.getText().toString().isEmpty()
                || !etKitchen.getText().toString().isEmpty() || !etColor.getText().toString().isEmpty() || !etCorridor.getText().toString().isEmpty()) {
            ApartmentDetails details = new ApartmentDetails();
            details.setSize(etSize.getText().toString());
            if (!etRoom.getText().toString().isEmpty())
                details.setRoom(Integer.parseInt(etRoom.getText().toString()));
            if (!etBath.getText().toString().isEmpty())
                details.setBath(Integer.parseInt(etBath.getText().toString()));
            if (!etKitchen.getText().toString().isEmpty())
                details.setKitchen(Integer.parseInt(etKitchen.getText().toString()));
            details.setColor(etColor.getText().toString());
            if (!etCorridor.getText().toString().isEmpty())
                details.setCorridor(Integer.parseInt(etCorridor.getText().toString()));
            apartment.setApartmentDetails(details);
        }
        Price price = new Price();
        if (updateApt != null) {
            if (updateApt.getPrice() != null) {
                price.setApartmentId(apartment.getAppartmentId());
                price.setPriceId(updateApt.getPrice().getPriceId());
            }
        }
        if (!tvElectricityCharges.getText().toString().isEmpty() || !tvGuardCharge.getText().toString().isEmpty()
                || !tvLiftCharge.getText().toString().isEmpty() || !tvWaterCharge.getText().toString().isEmpty() || !tvOthersCharge.getText().toString().isEmpty()) {
            PriceDetails priceDetails = new PriceDetails();
            if (updateApt != null) {
                if (updateApt.getPrice() != null) {
                    priceDetails.setPriceId(updateApt.getPrice().getPriceId());
                    if (updateApt.getPrice().getPriceDetails() != null) {
                        priceDetails.setPriceDetailsId(updateApt.getPrice().getPriceDetails().getPriceDetailsId());
                    }
                }
            }
            try {
                priceDetails.setElectricityCharge(Double.parseDouble(tvElectricityCharges.getText().toString()));
                priceDetails.setGaurdCharge(Double.parseDouble(tvGuardCharge.getText().toString()));
                priceDetails.setLiftCharge(Double.parseDouble(tvLiftCharge.getText().toString()));
                priceDetails.setWaterCharge(Double.parseDouble(tvWaterCharge.getText().toString()));
                priceDetails.setOthersCharge(Double.parseDouble(tvOthersCharge.getText().toString()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            price.setPriceDetails(priceDetails);
            if (!tvAmount.getText().toString().isEmpty())
                price.setAmount(Double.parseDouble(tvAmount.getText().toString()));
            else {
                showMessage("Price is required !");
                return;
            }
            apartment.setPrice(price);
        } else {
            if (!tvAmount.getText().toString().isEmpty()) {
                price.setAmount(Double.parseDouble(tvAmount.getText().toString()));
                apartment.setPrice(price);
            } else {
                showMessage("Price amount is required !");
                return;
            }
        }

        presenter.addApartment(apartment);

    }


    @Override
    public void setDivisionsToView(List<Divisions> divisions) {
        LinkedHashMap<Integer, String> items = new LinkedHashMap<>();
        items.put(0, "--Select Division--");
        for (Divisions dv : divisions) {
            items.put(dv.getDivisionId(), dv.getName());
        }
        generateSpinner(spnDivision, items);
        spnDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Map.Entry<Integer, String> data = (Map.Entry<Integer, String>) spnDivision.getSelectedItem();
                divId = data.getKey();
                if (divId != 0) {
                    commonPresenter.getDistricts(data.getKey());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        try {
            spnDivision.setSelection(getIndex(spnDivision, updateApt.getLocation().getUpazilas().getDistricts().getDivisionId()));
        } catch (Exception e) {
        }
    }

    @Override
    public void setDistrictsToView(List<Districts> districts) {
        LinkedHashMap<Integer, String> items = new LinkedHashMap<>();
        items.put(0, "--Select Districts--");
        for (Districts ds : districts) {
            items.put(ds.getDistrictId(), ds.getName());
        }
        generateSpinner(spnDistrict, items);
        spnDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Map.Entry<Integer, String> data = (Map.Entry<Integer, String>) spnDistrict.getSelectedItem();
                disId = data.getKey();
                if (disId != 0)
                    commonPresenter.getUpazilas(data.getKey());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        try {
            spnDistrict.setSelection(getIndex(spnDistrict, updateApt.getLocation().getUpazilas().getDistrictId()));
        } catch (Exception e) {
        }
    }

    @Override
    public void setUpazilasToView(List<Upazilas> upazilas) {
        LinkedHashMap<Integer, String> items = new LinkedHashMap<>();
        items.put(0, "--Select Upozila--");
        for (Upazilas up : upazilas) {
            items.put(up.getUpozilaId(), up.getName());
        }
        generateSpinner(spnUpazila, items);
        spnUpazila.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Map.Entry<Integer, String> data = (Map.Entry<Integer, String>) spnUpazila.getSelectedItem();
                upzId = data.getKey();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        try {
            spnUpazila.setSelection(getIndex(spnUpazila, updateApt.getLocation().getUpozilaId()));
        } catch (Exception e) {
        }
    }

    @Override
    public void adCreated(Object adId) {

    }

    private int uploadImageCount = 0;

    @Override
    public void aptCreated(int aptid) {
        runOnUiThread(() -> {
            showMessage("Success");
            this.selectedApartmentId = aptid;
            uploadImages();
        });
    }

    private void uploadImages() {
        if (attachments.size() > 0) {
            for (Attachments ats : attachments) {
                ats.setApartmentId(selectedApartmentId);
                ats.setFileName(selectedApartmentId + System.currentTimeMillis() + ".png");
                ats.setAttachType(AttachmentType.Photo);
                sendImage(ats.getFilePath(), ats);
                try {
                    Thread.sleep(TimeUnit.MILLISECONDS.toMillis(500));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            attachments.clear();
        } else {
            showMessage("No Image Selected !");
        }
    }

    @Override
    public void setUserApartmentToView(List<Apartment> apartments) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (data.getClipData() != null) {
                multipleSelect(data);
            } else if (data.getData() != null) {
                singleSelect(data);
            }
        }
    }

    private void singleSelect(final Intent data) {
        try {
            Uri uri = data.getData();
            File file = new File(getRealPathFromURI(uri));
            int fileSize = Integer.parseInt(String.valueOf(file.length() / 1024));
            if (fileSize > 1024) {
                showMessage("File is larger than one MB");
                return;
            }
            ImageView img = findViewById(R.id.imageOfApt);
            img.setImageURI(uri);
            Attachments attachment = new Attachments();
            attachment.setFilePath(uri);
            attachments.add(attachment);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void multipleSelect(Intent data) {
        try {
            for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                Uri uri = data.getClipData().getItemAt(i).getUri();
                File file = new File(getRealPathFromURI(uri));
                int fileSize = Integer.parseInt(String.valueOf(file.length() / 1024));
                if (fileSize > 1024) {
                    showMessage("File is larger than one MB");
                    return;
                }
                Attachments attachment = new Attachments();
                attachment.setFilePath(data.getClipData().getItemAt(i).getUri());
                attachments.add(attachment);
            }
        } catch (Exception e) {
            Log.d("E", e.toString());
        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    private void takePhotoFromFile() {
        imgCount = 0;
        if (Environment.getExternalStorageState().equals("mounted")) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            startActivityForResult(Intent.createChooser(intent, "Select Photo : "), 1);
        }
    }

    int imgCount = 0;

    @Override
    public void imageUploaded() {
        uploadImageCount++;
        showMessage("Image Uploaded !");
    }

    private void sendImage(Uri uri, Attachments attachment) {
        File file = new File(getRealPathFromURI(uri));
        RequestBody requestFile = RequestBody.create(MediaType.parse(getContentResolver().getType(uri)), file);
        MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), gson.toJson(attachment));
        presenter.postImages(multipartBody, body);
    }

    private void setAptDataToView(Apartment apartment) {
        if (apartment != null) {
            if (apartment.getTitle() != null) {
                etAptTitle.setText(apartment.getTitle());
            }
            if (apartment.getDescription() != null) {
                etDescription.setText(apartment.getDescription());
            }
            if (apartment.getLocation() != null) {
                etLocationName.setText(apartment.getLocation().getLocationName());
                etStreetAddress.setText(apartment.getLocation().getDetailsAddress());
            }
            Price price = apartment.getPrice();
            if (price != null) {
                tvAmount.setText(String.valueOf(price.getAmount()));
                if (price.getPriceDetails() != null) {
                    tvWaterCharge.setText(String.valueOf(price.getPriceDetails().getWaterCharge()));
                    tvElectricityCharges.setText(String.valueOf(price.getPriceDetails().getElectricityCharge()));
                    tvGuardCharge.setText(String.valueOf(price.getPriceDetails().getGaurdCharge()));
                    tvLiftCharge.setText(String.valueOf(price.getPriceDetails().getLiftCharge()));
                    tvOthersCharge.setText(String.valueOf(price.getPriceDetails().getOthersCharge()));
                }
            }

            if (apartment.getApartmentDetails() != null) {
                ApartmentDetails details = apartment.getApartmentDetails();
                etSize.setText(details.getSize());
                etRoom.setText(String.valueOf(details.getRoom()));
                etBath.setText(String.valueOf(details.getBath()));
                etKitchen.setText(String.valueOf(details.getKitchen()));
                etCorridor.setText(String.valueOf(details.getCorridor()));
                etColor.setText(details.getColor());
            }

        }
    }

    public void updateImageOnly(View view) {
        takePhotoFromFile();
        uploadImages();
    }

    public void updateImage() {
        int hasPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasPermission == PackageManager.PERMISSION_GRANTED) {
            takePhotoFromFile();
        } else {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    private void requestPermission(String permission) {
        String requestPermissionArray[] = {permission};
        ActivityCompat.requestPermissions(this, requestPermissionArray, READ_EXTERNAL_STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int length = grantResults.length;
        if (length > 0) {
            int grantResult = grantResults[0];
            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                takePhotoFromFile();
            } else {
                Toast.makeText(this, "You denied permission.", Toast.LENGTH_LONG).show();
            }
        }
    }
}
