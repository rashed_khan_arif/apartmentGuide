package com.finalproject.apartmentguide.ui.advertisement.apartment_details;

import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Review;
import com.finalproject.apartmentguide.ui.base.MvpView;

import java.util.List;


public interface AdvertisementDetailsMvpView extends MvpView {
    void setAdvertisementDetailsToView(Advertisement advertisement);

    void setAptReviewsToView(List<Review> reviews);

    void reviewDone();

    void reviewFailed();

}
