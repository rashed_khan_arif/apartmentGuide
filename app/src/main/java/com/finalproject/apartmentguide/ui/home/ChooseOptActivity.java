package com.finalproject.apartmentguide.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.model.AdType;
import com.finalproject.apartmentguide.ui.account.LoginActivity;
import com.finalproject.apartmentguide.ui.account.SignUpActivity;
import com.finalproject.apartmentguide.ui.advertisement.AdvertisementActivity;
import com.finalproject.apartmentguide.ui.advertisement.post_advertisement.CreateAdvertisementActivity;
import com.finalproject.apartmentguide.ui.base.BaseActivity;
import com.finalproject.apartmentguide.ui.user.UserActivity;

import javax.inject.Inject;

import static com.finalproject.apartmentguide.config.Constant.INTENT_CHOOSE_OPTION;

public class ChooseOptActivity extends BaseActivity {
    private TextView tvSignUp, tvLogin, tvLogOut, tvPostAd, tvBuy, tvRent;
    @Inject
    PreferenceHelper preferenceHelper;
    private LinearLayout llWithoutAuth, llAuth;
    private TextView tvProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_opt);
        APGApplication.getDefault().getApplicationComponent().inject(this);
        initialize();
        tvBuy.setOnClickListener(v -> {
            startActivity(new Intent(ChooseOptActivity.this, AdvertisementActivity.class).putExtra(INTENT_CHOOSE_OPTION, AdType.SELL.val));
            overridePendingTransition(R.anim.activity_slide_left, R.anim.activity_slide_right);
        });
        // Intent intent =new Intent(ChooseOptActivity.this, UserActivity.class);
        tvRent.setOnClickListener(v -> {
            Intent intent = new Intent(ChooseOptActivity.this, AdvertisementActivity.class).putExtra(INTENT_CHOOSE_OPTION, AdType.RENT.val);
            startActivity(intent);
            overridePendingTransition(R.anim.activity_slide_left, R.anim.activity_slide_right);
        });
        tvPostAd.setOnClickListener(v -> {
            if (APGApplication.getDefault().isLogin() && preferenceHelper.getUserInformation().getIsVerified()) {
                startActivity(new Intent(ChooseOptActivity.this, CreateAdvertisementActivity.class));
                overridePendingTransition(R.anim.activity_slide_left, R.anim.activity_slide_right);
            } else {
                showMessage("Please login to continue !");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (APGApplication.getDefault().isLogin() && preferenceHelper.getUserInformation().getIsVerified()) {
            llAuth.setVisibility(View.VISIBLE);
            llWithoutAuth.setVisibility(View.GONE);
            tvPostAd.setVisibility(View.VISIBLE);
        }
    }

    public void goToLogin(View view) {
        startActivity(new Intent(this, LoginActivity.class).putExtra("isInstantLogin", false));
    }

    public void goToSignUp(View view) {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    @Override
    public void initialize() {
        tvBuy = findViewById(R.id.tvBuy);
        tvRent = findViewById(R.id.tvRent);
        tvPostAd = findViewById(R.id.tvPostAd);
        tvLogin = findViewById(R.id.tvLogin);
        tvSignUp = findViewById(R.id.tvSignUp);
        tvLogOut = findViewById(R.id.tvLogOut);
        llAuth = findViewById(R.id.llAuth);
        tvProfile = findViewById(R.id.tvProfile);
        llWithoutAuth = findViewById(R.id.llWithoutAuth);
        bindComponents();
    }

    @Override
    public void bindComponents() {
        if (APGApplication.getDefault().isLogin() &&  preferenceHelper.getUserInformation().getIsVerified()) {
            llAuth.setVisibility(View.VISIBLE);
            tvPostAd.setVisibility(View.VISIBLE);
        }

        tvLogOut.setOnClickListener((view) -> {
            preferenceHelper.setUserInformation(null);
            startActivity(getIntent());
        });
        tvProfile.setOnClickListener((view) -> {
            startActivity(new Intent(ChooseOptActivity.this, UserActivity.class));
            overridePendingTransition(R.anim.activity_slide_left, R.anim.activity_slide_right);
        });
    }
}
