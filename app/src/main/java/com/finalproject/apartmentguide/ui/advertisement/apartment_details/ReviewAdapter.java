package com.finalproject.apartmentguide.ui.advertisement.apartment_details;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.model.Review;

import java.util.List;

public class ReviewAdapter extends BaseAdapter {
    private List<Review> reviews;
    private Context context;

    public ReviewAdapter(List<Review> reviews, Context context) {
        this.reviews = reviews;
        this.context = context;
    }

    @Override
    public int getCount() {
        return reviews.size();
    }

    @Override
    public Review getItem(int position) {
        return reviews.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getReviewId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.review_item_row, parent, false);
        } else
            view = convertView;
        TextView tvComments = view.findViewById(R.id.tvComments);
        RatingBar rtReview = view.findViewById(R.id.rtReview);
        if (getItem(position).getComments() != null) {
            tvComments.setText(getItem(position).getComments());
        }
        rtReview.setRating(getItem(position).getRating());


        return view;
    }
}
