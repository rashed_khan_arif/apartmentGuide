package com.finalproject.apartmentguide.ui.advertisement.post_advertisement;

import android.annotation.SuppressLint;

import com.finalproject.apartmentguide.core.ResponseParser;
import com.finalproject.apartmentguide.data.repository.AdvertisementRepository;
import com.finalproject.apartmentguide.data.repository.ApartmentRepository;
import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.Attachments;
import com.finalproject.apartmentguide.ui.base.BasePresenter;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class CreateAdPresenter<V extends CreateAdMvpView> extends BasePresenter<V> implements CreateAdMvpPresenter<V> {
    AdvertisementRepository advertisementRepository;
    ApartmentRepository apartmentRepository;

    @Inject
    public CreateAdPresenter(AdvertisementRepository advertisementRepository, ApartmentRepository apartmentRepository) {
        this.advertisementRepository = advertisementRepository;
        this.apartmentRepository = apartmentRepository;
    }

    @SuppressLint("CheckResult")
    @Override
    public void createAdvertisement(Advertisement advertisement) {
        getMvpView().showLoading();
        advertisementRepository.addAdvertisement(advertisement).subscribe(advertisement1 -> {
            getMvpView().hideLoading();
            getMvpView().adCreated(advertisement1);
        }, throwable -> {
            getMvpView().onError(ResponseParser.getErrorMessageFromResponse(throwable));
            getMvpView().hideLoading();
        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void getApartmentsByUserId(int id) {
        getMvpView().showLoading();
        apartmentRepository.getApartmentsByUserId(id).subscribe(advertisement1 -> {
            getMvpView().hideLoading();
            getMvpView().setUserApartmentToView(advertisement1);
        }, throwable -> {
            getMvpView().onError(ResponseParser.getErrorMessageFromResponse(throwable));
            getMvpView().hideLoading();
        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void addApartment(Apartment apartment) {
        getMvpView().showLoading();
        apartmentRepository.postApartment(apartment).subscribe(o -> {
            getMvpView().hideLoading();
            getMvpView().aptCreated(((Double) o).intValue());
        }, throwable -> {
            getMvpView().hideLoading();
            getMvpView().onError(ResponseParser.getErrorMessageFromResponse(throwable));
        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void uploadImage(String images) {
        apartmentRepository.postImages(images).subscribe(o -> {
            getMvpView().imageUploaded();
        }, throwable -> {
            getMvpView().showMessage(ResponseParser.getErrorMessageFromResponse(throwable));
        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void postImages(MultipartBody.Part part, RequestBody name) {
        apartmentRepository.postImages(part, name).subscribe(o -> {
            getMvpView().imageUploaded();
        }, throwable -> {
            getMvpView().showMessage(ResponseParser.getErrorMessageFromResponse(throwable));
        });
    }
}
