package com.finalproject.apartmentguide.ui.common;

import com.finalproject.apartmentguide.model.Districts;
import com.finalproject.apartmentguide.model.Divisions;
import com.finalproject.apartmentguide.model.Location;
import com.finalproject.apartmentguide.model.Upazilas;
import com.finalproject.apartmentguide.ui.base.MvpView;

import java.util.List;
 

public interface CommonMvpView extends MvpView {
    void setDivisionsToView(List<Divisions> divisions);

    void setDistrictsToView(List<Districts> districts);

    void setUpazilasToView(List<Upazilas> upazilas);

}
