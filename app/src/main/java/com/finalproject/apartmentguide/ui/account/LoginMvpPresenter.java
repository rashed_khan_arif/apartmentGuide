package com.finalproject.apartmentguide.ui.account;

import com.finalproject.apartmentguide.model.Credentials;
import com.finalproject.apartmentguide.model.Verify;
import com.finalproject.apartmentguide.ui.base.BasePresenter;
import com.finalproject.apartmentguide.ui.base.MvpPresenter;
import com.finalproject.apartmentguide.ui.base.MvpView;


public interface LoginMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
    void doLogin(Credentials credentials);

    void sendVerificationCode(int id);

    void checkVerificationCode(Verify body);
}
