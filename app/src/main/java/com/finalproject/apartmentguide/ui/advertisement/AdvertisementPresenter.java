package com.finalproject.apartmentguide.ui.advertisement;

import android.annotation.SuppressLint;

import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.common.PreferenceManager;
import com.finalproject.apartmentguide.core.ResponseParser;
import com.finalproject.apartmentguide.data.repository.AdvertisementRepository;
import com.finalproject.apartmentguide.model.AdType;
import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.ApartmentDetails;
import com.finalproject.apartmentguide.model.Location;
import com.finalproject.apartmentguide.ui.account.LoginMvpView;
import com.finalproject.apartmentguide.ui.base.BasePresenter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.functions.Consumer;


public class AdvertisementPresenter<V extends AdvertisementMvpView> extends BasePresenter<V> implements AdvertisementMvpPresenter<V> {
    AdvertisementRepository advertisementRepository;
    PreferenceHelper preferenceHelper;

    @Inject
    public AdvertisementPresenter(AdvertisementRepository advertisementRepository, PreferenceManager preferenceManager) {
        this.advertisementRepository = advertisementRepository;
        this.preferenceHelper = preferenceManager;
    }


    @SuppressLint("CheckResult")
    @Override
    public void doSearchApartments(int divId, int disId, int upzId, AdType adType, boolean top) {
        getMvpView().showLoading();
        advertisementRepository.getAdvertisements(divId, disId, upzId, adType, top).subscribe((List<Advertisement> advertisements) -> getMvpView().setAdvertisementsToView(advertisements), throwable -> {
            getMvpView().onError(ResponseParser.getErrorMessageFromResponse(throwable));
            getMvpView().hideLoading();
            getMvpView().noAdvertisementFound();
        }, () -> getMvpView().hideLoading());

    }

    @SuppressLint("CheckResult")
    @Override
    public void advertisementByUserId(int userId) {
        getMvpView().showLoading();
        advertisementRepository.getAdvertisementByUserID(userId).subscribe(advertisement -> {
            getMvpView().hideLoading();
            getMvpView().setAdvertisementsToUserView(advertisement);
        }, throwable -> {
            getMvpView().hideLoading();
            getMvpView().setAdvertisementsToUserView(new LinkedList<>());
        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void getApartmentsByUserId(int userId) {
        getMvpView().showLoading();
        advertisementRepository.getApartmentsByUserID(userId).subscribe(advertisement -> {
            getMvpView().hideLoading();
            getMvpView().setApartmentsToUserView(advertisement);
        }, throwable -> getMvpView().hideLoading());
    }

    @SuppressLint("CheckResult")
    @Override
    public void updateStatus(int adId, int status) {
        advertisementRepository.updateStatus(adId, status).subscribe(o -> {
            getMvpView().statusUpdated();
        }, throwable -> {
        });
    }
}
