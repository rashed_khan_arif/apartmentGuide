package com.finalproject.apartmentguide.ui.advertisement;

import com.finalproject.apartmentguide.model.AdType;
import com.finalproject.apartmentguide.ui.base.MvpPresenter;
import com.finalproject.apartmentguide.ui.base.MvpView;

public interface AdvertisementMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
    void doSearchApartments(int divId, int disId, int upzId, AdType adType, boolean top);

    void advertisementByUserId(int userId);

    void getApartmentsByUserId(int userId);

    void updateStatus(int adId, int status);
}
