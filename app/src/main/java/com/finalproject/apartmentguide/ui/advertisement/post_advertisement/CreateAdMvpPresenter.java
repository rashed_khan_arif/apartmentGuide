package com.finalproject.apartmentguide.ui.advertisement.post_advertisement;

import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.Attachments;
import com.finalproject.apartmentguide.ui.base.MvpPresenter;
import com.finalproject.apartmentguide.ui.base.MvpView;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public interface CreateAdMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
    void createAdvertisement(Advertisement advertisement);

    void getApartmentsByUserId(int id);

    void addApartment(Apartment apartment);

    void uploadImage(String images);

    void postImages(MultipartBody.Part part, RequestBody name);
}
