package com.finalproject.apartmentguide.ui.advertisement.apartment_details;

import android.annotation.SuppressLint;

import com.finalproject.apartmentguide.common.Util;
import com.finalproject.apartmentguide.core.ResponseParser;
import com.finalproject.apartmentguide.data.repository.AdvertisementRepository;
import com.finalproject.apartmentguide.data.repository.impl.AdvertisementRepositoryImpl;
import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Review;
import com.finalproject.apartmentguide.ui.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.functions.Consumer;


public class ApartmentDetailsPresenter<V extends AdvertisementDetailsMvpView> extends BasePresenter<V> implements AdvertisementDetailsMvpPresenter<V> {
    AdvertisementRepository repository;

    @Inject
    public ApartmentDetailsPresenter(AdvertisementRepositoryImpl repository) {
        this.repository = repository;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getDetails(int id) {
        getMvpView().showLoading();
        repository.getAdvertisement(id).subscribe(advertisement -> {
            getMvpView().setAdvertisementDetailsToView(advertisement);
        }, throwable -> {
            getMvpView().hideLoading();
            getMvpView().showMessage(ResponseParser.getErrorMessageFromResponse(throwable));
        }, () -> getMvpView().hideLoading());

    }

    @SuppressLint("CheckResult")
    @Override
    public void submitRateAndReview(Review body) {
        repository.submitRateAndReview(body).subscribe(o -> {
            getMvpView().showMessage("Success");
            getMvpView().reviewDone();
        }, throwable -> {
            getMvpView().showMessage(ResponseParser.getErrorMessageFromResponse(throwable));
            getMvpView().reviewFailed();
        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void getAptReviewsByAptId(int id) {
        repository.getReviewsByAptId(id).subscribe(reviews -> {
            getMvpView().setAptReviewsToView(reviews);
            if (reviews.size() == 0) {
                getMvpView().showMessage("No Review Found For This Apartment !");
            }
        }, throwable -> {
            getMvpView().showMessage(ResponseParser.getErrorMessageFromResponse(throwable));
        });
    }


}
