package com.finalproject.apartmentguide.ui.advertisement;

import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.config.Constant;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.model.AdType;
import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.Districts;
import com.finalproject.apartmentguide.model.Divisions;
import com.finalproject.apartmentguide.model.Location;
import com.finalproject.apartmentguide.model.Upazilas;
import com.finalproject.apartmentguide.ui.account.LoginActivity;
import com.finalproject.apartmentguide.ui.advertisement.apartment_details.AdvertisementDetailsActivity;
import com.finalproject.apartmentguide.ui.base.BaseActivity;
import com.finalproject.apartmentguide.ui.common.CommonMvpView;
import com.finalproject.apartmentguide.ui.common.CommonPresenter;
import com.finalproject.apartmentguide.ui.home.HomeActivity;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class AdvertisementActivity extends BaseActivity implements AdvertisementMvpView, CommonMvpView {
    private static final int REQUEST_SIGN_IN = 100;
    private RecyclerView rcvApartments;
    private FloatingActionButton fbtnSearch;
    private Spinner spnDivision, spnDistrict, spnUpozila, spnLocation, spnAdType;
    private int divId, disId, upzId;
    private AdType adType;
    Dialog dialog;
    @Inject
    AdvertisementMvpPresenter<AdvertisementMvpView> presenter;
    @Inject
    CommonPresenter<CommonMvpView> commonPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement);
        APGApplication.getDefault().getApplicationComponent().inject(this);
        presenter.onAttach(this);
        commonPresenter.onAttach(this);
        initialize();
        bindComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.doSearchApartments(0, 0, 0, adType, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.search:
                searchPage();
                break;
            case R.id.mnHome:
                startActivity(new Intent(this, HomeActivity.class));
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGN_IN && resultCode == RESULT_OK) {
            showMessage("Done");
        }
    }

    private void searchPage() {
        dialog = new Dialog(this);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_search_for_dialog, null);
        dialog.setContentView(view);
        dialog.show();
        if (dialog.isShowing()) {
            commonPresenter.getDivisions();
        }
        Button btnSearch = view.findViewById(R.id.btnSearch);
        spnAdType = view.findViewById(R.id.spnAdType);
        spnDivision = view.findViewById(R.id.spnDivision);
        spnDistrict = view.findViewById(R.id.spnDistrict);
        spnUpozila = view.findViewById(R.id.spnUpozila);

        btnSearch.setOnClickListener(v -> {
            if (divId == 0 && disId == 0 && upzId == 0) {
                showMessage("Please select a division");
                return;
            }
            presenter.doSearchApartments(divId, disId, upzId, adType, false);
        });

        LinkedHashMap<Integer, String> items = new LinkedHashMap<>();
        items.put(0, "--Select Type--");
        items.put(AdType.RENT.val, AdType.RENT.name());
        items.put(AdType.SELL.val, AdType.SELL.name());
        generateSpinner(spnAdType, items);
        spnAdType.setSelection(BaseActivity.getIndex(spnAdType, adType.val));
        spnAdType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Map.Entry<Integer, String> data = (Map.Entry<Integer, String>) spnAdType.getSelectedItem();
                adType = data.getKey() == 1 ? AdType.SELL : AdType.RENT;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void initialize() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        rcvApartments = findViewById(R.id.rcvApartments);
        fbtnSearch = findViewById(R.id.fbtnSearch);
    }

    @Override
    public void bindComponents() {
        int ad = getIntent().getIntExtra(Constant.INTENT_CHOOSE_OPTION, -1);
        if (ad != -1)
            adType = AdType.getFromInt(ad);

        // generateSpinner();
        setAdvertisementsToView(null);
        fbtnSearch.setOnClickListener(v -> {
            searchPage();
        });


    }

    @Override
    public void setAdvertisementsToView(List<Advertisement> advertisements) {
        if (dialog != null) {
            dialog.dismiss();
        }
        AdvertisementAdapter adapter = new AdvertisementAdapter(advertisements, this);
        rcvApartments.setAdapter(adapter);
        rcvApartments.setLayoutManager(new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL));
        rcvApartments.setItemAnimator(new DefaultItemAnimator());
        adapter.onItemClick(new AdvertisementAdapter.OnItemClick() {
            @Override
            public void onClick(int itemId) {
                Intent intent = new Intent(AdvertisementActivity.this, AdvertisementDetailsActivity.class);
                intent.putExtra(Constant.INTENT_INT_EXTRA, itemId);
                startActivity(intent);
            }

            @Override
            public void updateStatus(int adId, int status) {
                updateStatusOfAd(adId, status);
            }
        });
    }

    private void updateStatusOfAd(int adId, int status) {

    }

    @Override
    public void statusUpdated() {

    }

    @Override
    public void setAdvertisementsToUserView(List<Advertisement> advertisements) {

    }

    @Override
    public void setApartmentsToUserView(List<Apartment> advertisements) {

    }

    @Override
    public void noAdvertisementFound() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void setDivisionsToView(List<Divisions> divisions) {
        LinkedHashMap<Integer, String> items = new LinkedHashMap<>();
        items.put(0, "--Select Division--");
        for (Divisions dv : divisions) {
            items.put(dv.getDivisionId(), dv.getName());
        }
        generateSpinner(spnDivision, items);
        spnDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Map.Entry<Integer, String> data = (Map.Entry<Integer, String>) spnDivision.getSelectedItem();
                divId = data.getKey();
                if (divId != 0)
                    commonPresenter.getDistricts(data.getKey());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void setDistrictsToView(List<Districts> districts) {
        LinkedHashMap<Integer, String> items = new LinkedHashMap<>();
        items.put(0, "--Select Districts--");
        for (Districts ds : districts) {
            items.put(ds.getDistrictId(), ds.getName());
        }
        generateSpinner(spnDistrict, items);
        spnDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Map.Entry<Integer, String> data = (Map.Entry<Integer, String>) spnDistrict.getSelectedItem();
                disId = data.getKey();
                if (disId != 0)
                    commonPresenter.getUpazilas(data.getKey());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void setUpazilasToView(List<Upazilas> upazilas) {
        LinkedHashMap<Integer, String> items = new LinkedHashMap<>();
        items.put(0, "--Select Upozila--");
        for (Upazilas up : upazilas) {
            items.put(up.getUpozilaId(), up.getName());
        }
        generateSpinner(spnUpozila, items);
        spnUpozila.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Map.Entry<Integer, String> data = (Map.Entry<Integer, String>) spnUpozila.getSelectedItem();
                upzId = data.getKey();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}

