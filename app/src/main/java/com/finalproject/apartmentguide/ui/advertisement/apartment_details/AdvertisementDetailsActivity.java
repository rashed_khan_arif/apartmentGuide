package com.finalproject.apartmentguide.ui.advertisement.apartment_details;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.config.ApiEndPoints;
import com.finalproject.apartmentguide.config.Constant;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.model.AdType;
import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.ApartmentDetails;
import com.finalproject.apartmentguide.model.Attachments;
import com.finalproject.apartmentguide.model.Location;
import com.finalproject.apartmentguide.model.Price;
import com.finalproject.apartmentguide.model.Review;
import com.finalproject.apartmentguide.ui.base.BaseActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


import java.util.List;

import javax.inject.Inject;

public class AdvertisementDetailsActivity extends BaseActivity implements AdvertisementDetailsMvpView, OnMapReadyCallback {
    private int advertisementId;
    private TextView tvAptPrice, tvAptAddress, tvAptTitle, tvAptType,
            tvAptDetailsAddress, tvAvlFor, tvWaterCharge, tvGuardCharge, tvLiftCharge, tvElectricityCharges,
            tvOthersCharge, tvSize, tvRoom, tvKitchen, tvBath, tvCorridor, tvColor, tvOwnerName, tvOwnerContactNumber;
    private CardView cvPriceDetails, cvAptDetails, cvOwner;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    ActionBar actionBar;
    private ImageView imgImage;
    @Inject
    AdvertisementDetailsMvpPresenter<AdvertisementDetailsMvpView> presenter;
    @Inject
    PreferenceHelper preferenceHelper;
    private Advertisement advertisement;
    @Inject
    Gson gson;
    Dialog dialog;
    private Button btnCall;
    private int CALL_PER_REQ_CODE = 10;

    private SliderLayout imageSlider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement_details);
        advertisementId = getIntent().getIntExtra(Constant.INTENT_INT_EXTRA, -1);
        APGApplication.getDefault().getApplicationComponent().inject(this);
        initialize();
        bindComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onAttach(this);
        presenter.getDetails(advertisementId);

    }

    @Override
    public void initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnCall = findViewById(R.id.btnCall);
        imgImage = findViewById(R.id.imgImage);
        cvOwner = findViewById(R.id.cvOwner);
        cvAptDetails = findViewById(R.id.cvAptDetails);
        cvPriceDetails = findViewById(R.id.cvPriceDetails);
        tvOthersCharge = findViewById(R.id.tvOthersCharge);
        tvElectricityCharges = findViewById(R.id.tvElectricityCharges);
        tvLiftCharge = findViewById(R.id.tvLiftCharge);
        tvGuardCharge = findViewById(R.id.tvGuardCharge);
        tvWaterCharge = findViewById(R.id.tvWaterCharge);
        tvAvlFor = findViewById(R.id.tvAvlFor);
        tvAptDetailsAddress = findViewById(R.id.tvAptDetailsAddress);
        tvAptType = findViewById(R.id.tvAptType);
        tvAptAddress = findViewById(R.id.tvAptAddress);
        tvAptPrice = findViewById(R.id.tvAptPrice);
        tvAptTitle = findViewById(R.id.tvAptTitle);
        tvSize = findViewById(R.id.tvSize);
        tvRoom = findViewById(R.id.tvRoom);
        tvKitchen = findViewById(R.id.tvKitchen);
        tvBath = findViewById(R.id.tvBath);
        tvCorridor = findViewById(R.id.tvCorridor);
        tvColor = findViewById(R.id.tvColor);
        tvOwnerName = findViewById(R.id.tvOwnerName);
        imageSlider = findViewById(R.id.imageSlider);
        tvOwnerContactNumber = findViewById(R.id.tvOwnerContactNumber);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.frgMap);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_apartment_details, menu);
        return true;
    }

    @Override
    public void bindComponents() {
        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Apartment");
        mapFragment.getMapAsync(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onDetach();
    }

    @Override
    public void setAdvertisementDetailsToView(Advertisement advertisement) {
        this.advertisement = advertisement;
        if (advertisement.getTitle() != null) {
            tvAptTitle.setText(advertisement.getTitle());
        }
        if (advertisement.getApartment() != null) {
            if (advertisement.getApartment().getLocation() != null) {
                tvAptAddress.setText(advertisement.getApartment().getLocation().getLocationName());
                tvAptDetailsAddress.setText(advertisement.getApartment().getLocation().getDetailsAddress());
                setLocationToMap(advertisement.getApartment().getLocation(), advertisement.getTitle());
            }
            Price price = advertisement.getApartment().getPrice();
            if (price != null) {
                tvAptPrice.setText(String.valueOf(price.getAmount()));
                if (advertisement.getAdType() == AdType.RENT && price.getPriceDetails() != null) {
                    tvWaterCharge.setText(String.valueOf(price.getPriceDetails().getWaterCharge()));
                    tvElectricityCharges.setText(String.valueOf(price.getPriceDetails().getElectricityCharge()));
                    tvGuardCharge.setText(String.valueOf(price.getPriceDetails().getGaurdCharge()));
                    tvLiftCharge.setText(String.valueOf(price.getPriceDetails().getLiftCharge()));
                    tvOthersCharge.setText(String.valueOf(price.getPriceDetails().getOthersCharge()));
                } else cvPriceDetails.setVisibility(View.GONE);
            }

            if (advertisement.getApartment().getAttachments() != null) {
                for (Attachments attachments : advertisement.getApartment().getAttachments()) {
                    //attachments.isTitle()
                    String url = ApiEndPoints.getApartmentImageUrl(attachments.getFileName());
                    SliderView sliderView = new SliderView(this);
                    sliderView.setImageUrl(url);
                    sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
                    //sliderView.setDescription("setDescription " + (i + 1));
                    imageSlider.addSliderView(sliderView);
                }
            } else {
                imageSlider.setVisibility(View.GONE);
            }
            if (advertisement.getApartment().getApartmentDetails() != null) {
                ApartmentDetails details = advertisement.getApartment().getApartmentDetails();
                tvSize.setText(details.getSize());
                tvRoom.setText(String.valueOf(details.getRoom()));
                tvBath.setText(String.valueOf(details.getBath()));
                tvKitchen.setText(String.valueOf(details.getKitchen()));
                tvCorridor.setText(String.valueOf(details.getCorridor()));
                tvColor.setText(details.getColor());
            } else cvAptDetails.setVisibility(View.GONE);
            if (!APGApplication.getDefault().isLogin()) {
                cvOwner.setVisibility(View.GONE);
            }
            if (advertisement.getApartment().getUser() == null) {
                cvOwner.setVisibility(View.GONE);
            } else {
                tvOwnerContactNumber.setText(String.valueOf(advertisement.getApartment().getUser().getCellNumber()));
                tvOwnerName.setText(advertisement.getApartment().getUser().getFullName());
                String cellNumber = advertisement.getApartment().getUser().getCellNumber();
                if (APGApplication.getDefault().isLogin() && cellNumber != null && cellNumber.length() >= 10) {
                    btnCall.setVisibility(View.VISIBLE);
                }
            }
        }
        if (advertisement.getAdType() != null) {
            tvAptType.setText(advertisement.getAdType().name());
            actionBar.setSubtitle(advertisement.getAdType().name());
            if (advertisement.getAdType() == AdType.RENT) {
                cvPriceDetails.setVisibility(View.VISIBLE);
            }
        }
        if (advertisement.getAvailableFor() != null) {
            tvAvlFor.setText(advertisement.getAvailableFor().name());
        }

    }

    @Override
    public void setAptReviewsToView(List<Review> reviews) {
        runOnUiThread(() -> {
            if (reviews.size() == 0)
                return;
            Dialog dialog = new Dialog(this, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
            View v = LayoutInflater.from(this).inflate(R.layout.read_reviews_layout, null);
            dialog.setContentView(v);
            dialog.show();
            ListView lvReadReviews = v.findViewById(R.id.lvReadReviews);
            ReviewAdapter ra = new ReviewAdapter(reviews, this);
            lvReadReviews.setAdapter(ra);
        });
    }

    private void setLocationToMap(Location location, String title) {
        if (map != null)
            if (location.getDetailsAddress() != null) {
                LatLng latLng = getLatLng(location.getDetailsAddress());
                if (latLng == null) {
                    showMessage("Address can not recognise !");
                    return;
                }
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                map.addMarker(new MarkerOptions().position(latLng).title(title)).showInfoWindow();
            } else if (location.getStreetAddress() != null) {

                LatLng latLng = getLatLng(location.getStreetAddress());
                if (latLng == null) {
                    showMessage("Address can not recognise !");
                    return;
                }
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                map.addMarker(new MarkerOptions().position(latLng).title(title)).showInfoWindow();
            } else if (location.getLocationName() != null) {

                LatLng latLng = getLatLng(location.getLocationName());
                if (latLng == null) {
                    showMessage("Address can not recognise !");
                    return;
                }
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                map.addMarker(new MarkerOptions().position(latLng).title(title)).showInfoWindow();
            }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setMapToolbarEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setAllGesturesEnabled(true);

    }

    public LatLng getLatLng(String address) {
        Geocoder geocoder = new Geocoder(this);
        List<Address> addresses = null;
        LatLng latLng = null;
        try {
            addresses = geocoder.getFromLocationName(address, 5);

            latLng = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return latLng;
    }

    public void writeReview(View view) {
        dialog = new Dialog(this, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
        View v = LayoutInflater.from(this).inflate(R.layout.review_layout, null);
        dialog.setContentView(v);
        dialog.show();
        EditText etComments = v.findViewById(R.id.etComments);
        RatingBar rtAptRating = v.findViewById(R.id.rtAptRating);
        TextView tvClose = v.findViewById(R.id.tvClose);
        Button btnSubmit = v.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(v1 -> {
            Review review = new Review();
            review.setComments(etComments.getText().toString());
            review.setReviewedUserId(preferenceHelper.getUserInformation().getUserId());
            review.setApartmentId(advertisement.getApartmentId());
            review.setRating(rtAptRating.getRating());
            presenter.submitRateAndReview(review);
        });
        tvClose.setOnClickListener(v12 -> {
            dialog.dismiss();
        });
    }

    @Override
    public void reviewDone() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    @Override
    public void reviewFailed() {

    }

    public void readReview(View view) {
        presenter.getAptReviewsByAptId(advertisement.getApartmentId());
    }

    public void callToOwner(View view) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CALL_PER_REQ_CODE);
        } else {
            callToPhoneNumber();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CALL_PER_REQ_CODE && grantResults.length > 0) {
            callToPhoneNumber();
        }
    }

    private void callToPhoneNumber() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + advertisement.getApartment().getUser().getCellNumber()));//change the number.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }
}
