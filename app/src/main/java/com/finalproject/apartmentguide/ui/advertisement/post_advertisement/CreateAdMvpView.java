package com.finalproject.apartmentguide.ui.advertisement.post_advertisement;

import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.ui.base.MvpView;

import java.util.List;
 

public interface CreateAdMvpView extends MvpView {
    void adCreated(Object adId);

    void aptCreated(int aptid);

    void setUserApartmentToView(List<Apartment> apartments);

    void imageUploaded();
}
