package com.finalproject.apartmentguide.ui.user;

import android.annotation.SuppressLint;

import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.common.PreferenceManager;
import com.finalproject.apartmentguide.common.Util;
import com.finalproject.apartmentguide.core.ResponseParser;
import com.finalproject.apartmentguide.data.repository.AdvertisementRepository;
import com.finalproject.apartmentguide.data.repository.UserRepository;
import com.finalproject.apartmentguide.model.User;
import com.finalproject.apartmentguide.ui.base.BasePresenter;

import javax.inject.Inject;


public class UserPresenter<V extends UserMvpView> extends BasePresenter<V> implements UserMvpPresenter<V> {
    UserRepository repository;
    PreferenceHelper preferenceHelper;

    @Inject
    public UserPresenter(UserRepository repository, PreferenceManager preferenceManager) {
        this.repository = repository;
        this.preferenceHelper = preferenceManager;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getUserDetails(int userId) {
        getMvpView().showLoading();
        repository.getUserDetails(userId).subscribe(user -> {
            getMvpView().setUserdataToView(user);
            getMvpView().hideLoading();
        }, throwable -> {
            getMvpView().showMessage(ResponseParser.getErrorMessageFromResponse(throwable));
            getMvpView().hideLoading();
        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void updateUserInfo(User user) {
        getMvpView().showLoading();

        repository.updateUser(user).subscribe(u -> {
            getMvpView().userUpdated(u);
            getMvpView().hideLoading();
        }, throwable -> {
            getMvpView().hideLoading();
            getMvpView().showMessage(ResponseParser.getErrorMessageFromResponse(throwable));
        });
    }
}
