package com.finalproject.apartmentguide.ui.advertisement;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.common.BaseHolder;
import com.finalproject.apartmentguide.common.EmptyHolder;
import com.finalproject.apartmentguide.config.ApiEndPoints;
import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.Attachments;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Collection;
import java.util.List;

public class AdvertisementAdapter extends RecyclerView.Adapter<BaseHolder> {
    private boolean aptOwner;
    private List<Advertisement> advertisements;
    private Context context;
    private static final int EMPTY = 60;
    private static final int ITEM = 562;

    public AdvertisementAdapter(List<Advertisement> advertisements, Context context) {
        this.advertisements = advertisements;
        this.context = context;
    }

    public AdvertisementAdapter(List<Advertisement> advertisements, Context context, boolean aptOwner) {
        this.advertisements = advertisements;
        this.aptOwner = aptOwner;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (advertisements == null || advertisements.size() == 0) {
            return EMPTY;
        } else return ITEM;
    }

    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == EMPTY) {
            return new EmptyHolder(LayoutInflater.from(context).inflate(R.layout.layout_empty_item, parent, false));
        } else if (viewType == ITEM) {
            if (!aptOwner) {
                return new AdvHolder(LayoutInflater.from(context).inflate(R.layout.layout_advertisement_item, parent, false));
            }
            return new AdvHolder(LayoutInflater.from(context).inflate(R.layout.layout_advertisement_item_horizontal, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        if (holder instanceof AdvHolder) {
            AdvHolder advHolder = (AdvHolder) holder;
            if (aptOwner) {
                advHolder.adStatus.setChecked(advertisements.get(position).getIsPublished()==1);
                advHolder.adStatus.setText(advHolder.adStatus.isChecked() ? "Active" : "Inactive");
                advHolder.adStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isChecked = advHolder.adStatus.isChecked();
                        itemClick.updateStatus(advertisements.get(position).getAdvertisementId(), isChecked ? 1 : 0);
                    }
                });
            }
            advHolder.tvAdvTitle.setText(advertisements.get(position).getTitle());
            Apartment apartment = advertisements.get(position).getApartment();
            if (apartment != null) {
                if (apartment.getLocation() != null) {
                    if (apartment.getLocation().getDetailsAddress() != null) {
                        advHolder.tvAptAddress.setText(advertisements.get(position).getApartment().getLocation().getDetailsAddress());
                    }
                    if (apartment.getPrice() != null) {
                        advHolder.tvAptPrice.setText(String.valueOf(apartment.getPrice().getAmount()));
                    }
                }
                if (apartment.getAttachments() != null) {
                    for (Attachments attachments : apartment.getAttachments()) {
                        //attachments.isTitle()
                        String url = ApiEndPoints.getApartmentImageUrl(attachments.getFileName());
                        Picasso.with(context).load(url).into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                advHolder.imgApartShortImage.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });
                    }
                }
            }
            if (advertisements.get(position).getAdType() != null) {
                advHolder.tvType.setText(advertisements.get(position).getAdType().name());
            }

        }
    }

    OnItemClick itemClick;

    public void onItemClick(OnItemClick onItemClick) {
        itemClick = onItemClick;
    }

    @Override
    public int getItemCount() {
        if (advertisements != null && advertisements.size() > 0) {
            return advertisements.size();
        }
        return 1;

    }

    public class AdvHolder extends BaseHolder implements View.OnClickListener {
        private ImageView imgApartShortImage;
        private TextView tvAdvTitle, tvAptAddress, tvAptPrice, tvType;
        private Switch adStatus;

        public AdvHolder(View itemView) {
            super(itemView);
            imgApartShortImage = itemView.findViewById(R.id.imgApartShortImage);
            tvAdvTitle = itemView.findViewById(R.id.tvAdvTitle);
            tvAptAddress = itemView.findViewById(R.id.tvAptAddress);
            tvAptPrice = itemView.findViewById(R.id.tvAptPrice);
            tvType = itemView.findViewById(R.id.tvType);
            adStatus = itemView.findViewById(R.id.adStatus);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClick.onClick(advertisements.get(getAdapterPosition()).getAdvertisementId());
        }
    }

    public interface OnItemClick {
        void onClick(int itemId);

        void updateStatus(int adId, int status);
    }

}
