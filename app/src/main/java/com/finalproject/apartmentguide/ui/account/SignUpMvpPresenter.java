package com.finalproject.apartmentguide.ui.account;

import com.finalproject.apartmentguide.model.Credentials;
import com.finalproject.apartmentguide.model.User;
import com.finalproject.apartmentguide.ui.base.MvpPresenter;
import com.finalproject.apartmentguide.ui.base.MvpView;

 

public interface SignUpMvpPresenter<V extends MvpView> extends MvpPresenter<V>{
    void doSignUp(Credentials credentials);
}
