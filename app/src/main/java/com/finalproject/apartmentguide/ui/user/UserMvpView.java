package com.finalproject.apartmentguide.ui.user;

import com.finalproject.apartmentguide.model.User;
import com.finalproject.apartmentguide.ui.base.MvpView;

public interface UserMvpView extends MvpView {
    void setUserdataToView(User userdata);

    void updateUserClick(User user);

    void userUpdated(User user);

    void userNotUpdated();
}
