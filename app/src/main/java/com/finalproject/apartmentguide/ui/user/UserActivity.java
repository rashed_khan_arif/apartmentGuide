package com.finalproject.apartmentguide.ui.user;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.model.User;
import com.finalproject.apartmentguide.model.UserType;
import com.finalproject.apartmentguide.ui.advertisement.AdvertisementListActivity;
import com.finalproject.apartmentguide.ui.base.BaseActivity;
import com.finalproject.apartmentguide.ui.home.HomeActivity;

import javax.inject.Inject;

public class UserActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, UserMvpView {
    private TextView tvApartments, tvZipCode, tvCity, tvStreetAddress, tvEmail, tvPhone, tvFullName, tvUserType;
    private ImageView ivIUserImage;
    private CardView cvTotalApt;
    @Inject
    UserMvpPresenter<UserMvpView> presenter;
    @Inject
    PreferenceHelper preferenceHelper;
    private Dialog updateUserDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        APGApplication.getDefault().getApplicationComponent().inject(this);
        initialize();
        bindComponents();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onAttach(this);
        presenter.getUserDetails(preferenceHelper.getUserInformation().getUserId());
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onDetach();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.home) {
            startActivity(new Intent(this, HomeActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.navApts) {
            startActivity(new Intent(this, AdvertisementListActivity.class).putExtra("viewType", 2));
        } else if (id == R.id.navAdverts) {
            startActivity(new Intent(this, AdvertisementListActivity.class).putExtra("viewType", 3));
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void initialize() {
        FloatingActionButton fab = findViewById(R.id.fab);
        cvTotalApt = findViewById(R.id.cvTotalApt);
        tvApartments = findViewById(R.id.tvApartments);
        tvZipCode = findViewById(R.id.tvZipCode);
        tvCity = findViewById(R.id.tvCity);
        tvStreetAddress = findViewById(R.id.tvStreetAddress);
        tvEmail = findViewById(R.id.tvEmail);
        tvPhone = findViewById(R.id.tvPhone);
        ivIUserImage = findViewById(R.id.ivIUserImage);
        tvFullName = findViewById(R.id.tvFullName);
        fab.setOnClickListener(v -> {
            updateUserClick(preferenceHelper.getUserInformation());
        });
        bindComponents();

    }

    @Override
    public void bindComponents() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        TextView tvUserName = navigationView.getHeaderView(0).findViewById(R.id.tvUserName);
        TextView tvEmail = navigationView.getHeaderView(0).findViewById(R.id.tvEmail);
        tvUserType = navigationView.getHeaderView(0).findViewById(R.id.tvUserType);
        if (preferenceHelper.getUserInformation() != null) {
            tvUserName.setText(preferenceHelper.getUserInformation().getFullName());
            tvEmail.setText(preferenceHelper.getUserInformation().getEmail());
            if (preferenceHelper.getUserInformation().getUserRole() != null)
                tvUserType.setText(preferenceHelper.getUserInformation().getUserRole().getUserRoleId() == UserType.SELLER.val ? "Apartment Owner" : "Customer");
        }
    }

    @Override
    public void setUserdataToView(User userdata) {
        runOnUiThread(() -> {

            if (userdata != null) {
                if (userdata.getFullName() != null) {
                    tvFullName.setText(userdata.getFullName());
                }
                if (userdata.getAddress() != null) {
                    tvStreetAddress.setText(userdata.getAddress());
                }
                if (userdata.getCellNumber() != null) {
                    tvPhone.setText(userdata.getCellNumber());
                }
                if (userdata.getCity() != null) {
                    tvCity.setText(userdata.getCity());
                }
                if (userdata.getEmail() != null) {
                    tvEmail.setText(userdata.getEmail());
                }
                if (userdata.getZipCode() != 0) {
                    tvZipCode.setText(String.valueOf(userdata.getZipCode()));
                }
                if (userdata.getUserRole() != null) {
                    if (userdata.getUserRole().getUserRoleId() == UserType.SELLER.val) {
                        cvTotalApt.setVisibility(View.VISIBLE);
                        if (userdata.getTotalApartments() != 0) {
                            tvApartments.setText(String.valueOf(userdata.getTotalApartments()));
                        }
                    }
                }

            }
        });
    }

    @Override
    public void updateUserClick(User userdata) {
        updateUserDialog = new Dialog(this);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_update_user, null);
        updateUserDialog.setContentView(view);
        updateUserDialog.show();

        EditText etFullName = view.findViewById(R.id.etFullName);
        EditText etAddress = view.findViewById(R.id.etAddress);
        EditText etEmail = view.findViewById(R.id.etEmail);
        EditText etCity = view.findViewById(R.id.etCity);
        EditText etPhone = view.findViewById(R.id.etPhone);
        EditText etZipCode = view.findViewById(R.id.etZipCode);
        Button btnUpdateUser = view.findViewById(R.id.btnUpdateUser);
        if (userdata.getFullName() != null) {
            etFullName.setText(userdata.getFullName());
        }
        if (userdata.getAddress() != null) {
            etAddress.setText(userdata.getAddress());
        }
        if (userdata.getCellNumber() != null) {
            etPhone.setText(userdata.getCellNumber());
        }
        if (userdata.getCity() != null) {
            etCity.setText(userdata.getCity());
        }
        if (userdata.getEmail() != null) {
            etEmail.setText(userdata.getEmail());
        }
        if (userdata.getZipCode() != 0) {
            etZipCode.setText(String.valueOf(userdata.getZipCode()));
        }
        btnUpdateUser.setOnClickListener(v -> {
            User user = userdata;
            user.setFullName(etFullName.getText().toString());
            user.setAddress(etAddress.getText().toString());
            user.setCellNumber(etPhone.getText().toString());
            user.setCity(etCity.getText().toString());
            if (!etZipCode.getText().toString().isEmpty())
                user.setZipCode(Integer.parseInt(etZipCode.getText().toString()));
            presenter.updateUserInfo(user);
        });

    }

    @Override
    public void userUpdated(User user) {
        preferenceHelper.setUserInformation(user);
        showMessage("User info updated !");
        updateUserDialog.dismiss();
        startActivity(getIntent());
    }

    @Override
    public void userNotUpdated() {

    }
}
