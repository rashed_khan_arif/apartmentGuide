package com.finalproject.apartmentguide.ui.home;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.ui.account.LoginActivity;

public class HomeActivity extends AppCompatActivity {
    TextView tvVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        tvVersion = findViewById(R.id.tvVersion);
        findViewById(R.id.btnStart).setOnClickListener(v -> {
            startActivity(new Intent(HomeActivity.this, ChooseOptActivity.class));
        });
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionName = info.versionName;
            tvVersion.setText("Version: ".concat(versionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

}
