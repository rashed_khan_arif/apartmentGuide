package com.finalproject.apartmentguide.ui;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.ui.home.HomeActivity;

public class SplashActivity extends AppCompatActivity {
    private TextView tvAptGuide;
    Animation slideImage,slideText;
    private ImageView imgLogoOfApt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
        configAnim();
    }

    private void configAnim() {
        slideImage = AnimationUtils.loadAnimation(this, R.anim.slide_left);
        slideText = AnimationUtils.loadAnimation(this, R.anim.activity_slide_right);
        tvAptGuide.startAnimation(slideText);
        imgLogoOfApt.startAnimation(slideImage);
        slideText.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                CountDownTimer timer = new CountDownTimer(2000, 1000) {

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    }
                };
                timer.start();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void init() {
        tvAptGuide = findViewById(R.id.tvAptGuide);
        imgLogoOfApt = findViewById(R.id.imgLogoOfApt);
    }
}
