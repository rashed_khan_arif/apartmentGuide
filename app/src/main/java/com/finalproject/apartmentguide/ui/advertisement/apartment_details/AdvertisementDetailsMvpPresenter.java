package com.finalproject.apartmentguide.ui.advertisement.apartment_details;

import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Review;
import com.finalproject.apartmentguide.ui.base.BasePresenter;
import com.finalproject.apartmentguide.ui.base.MvpPresenter;
import com.finalproject.apartmentguide.ui.base.MvpView;


public interface AdvertisementDetailsMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
    void getDetails(int id);

    void submitRateAndReview(Review review);

    void getAptReviewsByAptId(int id);


}
