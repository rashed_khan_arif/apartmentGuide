package com.finalproject.apartmentguide.ui.account;

import com.finalproject.apartmentguide.ui.base.MvpView;

interface VerifyView extends MvpView {
    void msgSent(Object obj);

    void userVerified();

    void userVerificationFailed(String msg);
}
