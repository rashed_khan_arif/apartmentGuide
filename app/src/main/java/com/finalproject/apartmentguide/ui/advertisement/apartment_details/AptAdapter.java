package com.finalproject.apartmentguide.ui.advertisement.apartment_details;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.model.Apartment;

import java.util.List;

public class AptAdapter extends BaseAdapter {
    private List<Apartment> apartments;
    private Context context;

    public AptAdapter(List<Apartment> apartments, Context context) {
        this.apartments = apartments;
        this.context = context;
    }

    @Override
    public int getCount() {
        return apartments.size();
    }

    @Override
    public Apartment getItem(int position) {
        return apartments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getAppartmentId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.apartment_row, parent, false);
        } else {
            view = convertView;
        }
        TextView tvAptTitle = view.findViewById(R.id.tvAptTitle);
        TextView tvAptAddress = view.findViewById(R.id.tvAptAddress);
        TextView tvAptPrice = view.findViewById(R.id.tvAptPrice);
        if (getItem(position).getTitle() != null) {
            tvAptTitle.setText(getItem(position).getTitle());
        }
        if (getItem(position).getLocation() != null) {
            if (getItem(position).getLocation() != null) {
                tvAptAddress.setText(getItem(position).getLocation().getLocationName());
            }
        }
        if (getItem(position).getPrice() != null) {
            tvAptPrice.setText(String.valueOf(getItem(position).getPrice().getAmount()));
        }

        return view;
    }
}
