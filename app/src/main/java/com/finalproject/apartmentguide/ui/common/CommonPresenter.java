package com.finalproject.apartmentguide.ui.common;

import com.finalproject.apartmentguide.core.ResponseParser;
import com.finalproject.apartmentguide.data.repository.CommonRepository;
import com.finalproject.apartmentguide.model.Divisions;
import com.finalproject.apartmentguide.ui.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
 

public class CommonPresenter<V extends CommonMvpView> extends BasePresenter<V> implements CommonMvpPresenter<V> {

    CommonRepository repository;

    @Inject
    public CommonPresenter(CommonRepository repository) {
        this.repository = repository;
    }

    @Override
    public void getDivisions() {
        getMvpView().showLoading();
        repository.getDivisions().subscribe(divisions -> {
            getMvpView().setDivisionsToView(divisions);
            getMvpView().hideLoading();
        }, throwable -> {
            getMvpView().onError(ResponseParser.getErrorMessageFromResponse(throwable));
            getMvpView().hideLoading();
        }, () -> getMvpView().hideLoading());
    }

    @Override
    public void getDistricts(int divId) {
        getMvpView().showLoading();
        repository.getDistricts(divId).subscribe(districts -> {
            getMvpView().setDistrictsToView(districts);
            getMvpView().hideLoading();
        }, throwable -> {
            getMvpView().onError(ResponseParser.getErrorMessageFromResponse(throwable));
            getMvpView().hideLoading();
        }, () -> getMvpView().hideLoading());
    }

    @Override
    public void getUpazilas(int disId) {
        getMvpView().showLoading();
        repository.getUpazilas(disId).subscribe(upazilas -> {
            getMvpView().setUpazilasToView(upazilas);
            getMvpView().hideLoading();
        }, throwable -> {
            getMvpView().onError(ResponseParser.getErrorMessageFromResponse(throwable));
            getMvpView().hideLoading();
        }, () -> getMvpView().hideLoading());
    }


}
