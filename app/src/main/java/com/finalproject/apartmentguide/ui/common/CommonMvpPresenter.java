package com.finalproject.apartmentguide.ui.common;

import com.finalproject.apartmentguide.ui.base.BasePresenter;
import com.finalproject.apartmentguide.ui.base.MvpPresenter;
import com.finalproject.apartmentguide.ui.base.MvpView;
 
public interface CommonMvpPresenter<V extends MvpView> extends MvpPresenter<V> {

    void getDivisions();

    void getDistricts(int divId);

    void getUpazilas(int disId);


}
