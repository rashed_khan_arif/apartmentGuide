package com.finalproject.apartmentguide.ui.account;

import android.content.Intent;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.config.Constant;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.model.AdType;
import com.finalproject.apartmentguide.model.Credentials;
import com.finalproject.apartmentguide.ui.advertisement.AdvertisementActivity;
import com.finalproject.apartmentguide.ui.base.BaseActivity;
import com.finalproject.apartmentguide.ui.home.HomeActivity;

import javax.inject.Inject;

public class LoginActivity extends BaseActivity implements LoginMvpView {

    private EditText mEmailView;
    private EditText mPasswordView;
    private TextView tvSignUp;
    private Button mEmailSignInButton;
    @Inject
    public LoginMvpPresenter<LoginMvpView> loginMvpPresenter;
    private boolean isInstantLogin;
    @Inject
    public PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        isInstantLogin = getIntent().getBooleanExtra("isInstantLogin", false);
        APGApplication.getDefault().getApplicationComponent().inject(this);
        loginMvpPresenter.onAttach(this);
        initialize();
        bindComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (APGApplication.getDefault().isLogin() && preferenceHelper.getUserInformation().getIsVerified()) {
            onBackPressed();
        }
    }

    @Override
    public void onLoginClick() {
        hideKeyboard();
        mEmailView.setError(null);
        mPasswordView.setError(null);
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            Credentials credentials = new Credentials();
            credentials.setEmail(email);
            credentials.setPassword(password);
            loginMvpPresenter.doLogin(credentials);
        }
    }

    @Override
    public void loginSuccess() {
        if (!preferenceHelper.getUserInformation().getIsVerified()) {
            startActivity(new Intent(this, VerificationActivity.class));
            return;
        }
        if (isInstantLogin) {
            Intent data = new Intent();
            setResult(RESULT_OK, data);
            finish();
        } else {
            startActivity(new Intent(this, AdvertisementActivity.class).putExtra(Constant.INTENT_CHOOSE_OPTION, AdType.RENT.val));
        }
    }

    @Override
    public void msgSent(Object obj) {

    }

    @Override
    public void userVerified() {

    }

    @Override
    public void userVerificationFailed(String msg) {

    }

    @Override
    public void signUpClick() {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    @Override
    public void initialize() {
        mEmailView = findViewById(R.id.email);
        mPasswordView = findViewById(R.id.password);
        mEmailSignInButton = (Button) findViewById(R.id.btnSignIn);
        tvSignUp = (TextView) findViewById(R.id.tvSignUp);
        setSupportActionBar(findViewById(R.id.toolbar));
    }

    @Override
    public void bindComponents() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Login");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        mPasswordView.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                onLoginClick();
                return true;
            }
            return false;
        });
        mEmailSignInButton.setOnClickListener(view -> onLoginClick());
        tvSignUp.setOnClickListener(v -> signUpClick());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }
}

