package com.finalproject.apartmentguide.ui.user;

import com.finalproject.apartmentguide.model.User;
import com.finalproject.apartmentguide.ui.base.MvpPresenter;
import com.finalproject.apartmentguide.ui.base.MvpView;

public interface UserMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
    void getUserDetails(int userId);

    void updateUserInfo(User user);
}
