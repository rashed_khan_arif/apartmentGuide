package com.finalproject.apartmentguide.ui.advertisement.post_advertisement;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.model.AdType;
import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.ApartmentDetails;
import com.finalproject.apartmentguide.model.AttachmentType;
import com.finalproject.apartmentguide.model.Attachments;
import com.finalproject.apartmentguide.model.AvailableFor;
import com.finalproject.apartmentguide.model.Districts;
import com.finalproject.apartmentguide.model.Divisions;
import com.finalproject.apartmentguide.model.Location;
import com.finalproject.apartmentguide.model.Price;
import com.finalproject.apartmentguide.model.PriceDetails;
import com.finalproject.apartmentguide.model.Upazilas;
import com.finalproject.apartmentguide.ui.base.BaseActivity;
import com.finalproject.apartmentguide.ui.common.CommonMvpView;
import com.finalproject.apartmentguide.ui.common.CommonPresenter;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class CreateAdvertisementActivity extends BaseActivity implements CreateAdMvpView {
    private TextView tvCreateApartment;
    private EditText etAdTitle;
    private RadioGroup rdAvlFor;
    private CheckBox chkPublish;
    private Spinner spnSelectApartment, spnAdType;

    @Inject
    CreateAdMvpPresenter<CreateAdMvpView> presenter;
    @Inject
    PreferenceHelper preferenceHelper;
    private int selectedApartmentId;
    private int adType;
    @Inject
    Gson gson;
    int userId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_advertisement);
        initialize();
        bindComponents();
    }

    @Override
    public void initialize() {
        APGApplication.getDefault().getApplicationComponent().inject(this);
        setSupportActionBar(findViewById(R.id.toolbar));
        etAdTitle = findViewById(R.id.etAdTitle);
        tvCreateApartment = findViewById(R.id.tvCreateApartment);
        chkPublish = findViewById(R.id.chkPublish);
        rdAvlFor = findViewById(R.id.rdAvlFor);
        spnAdType = findViewById(R.id.spnAdType);
        spnSelectApartment = findViewById(R.id.spnSelectApartment);
        Button btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(v -> {
            saveAdvertise();
        });
    }

    private void saveAdvertise() {
        Advertisement advertisement = new Advertisement();
        if (selectedApartmentId == 0) {
            showMessage("Please select your apartment !");
            return;
        }
        if (etAdTitle.getText().toString().isEmpty()) {
            etAdTitle.setError("Title is required !");
            return;
        }
        if (adType == 0) {
            showMessage("Please select an ad type !");
            return;
        }
        AvailableFor avl = null;
        if (adType == AdType.SELL.val) {
            avl = AvailableFor.Both;
        } else {
            int id = rdAvlFor.getCheckedRadioButtonId();
            if (id == R.id.family) {
                avl = AvailableFor.Family;
            } else if (id == R.id.bachelor) {
                avl = AvailableFor.Bachelor;
            } else if (id == R.id.office) {
                avl = AvailableFor.Office;
            } else {
                avl = AvailableFor.Both;
            }
        }
        advertisement.setTitle(etAdTitle.getText().toString());
        advertisement.setAdType(adType == 1 ? AdType.SELL : AdType.RENT);
        advertisement.setIsPublished(chkPublish.isChecked() ? 1 : 0);
        advertisement.setApartmentId(selectedApartmentId);
        advertisement.setAvailableFor(avl);
        advertisement.setUserId(preferenceHelper.getUserInformation().getUserId());
        presenter.createAdvertisement(advertisement);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onAttach(this);
        userId = preferenceHelper.getUserInformation().getUserId();
        presenter.getApartmentsByUserId(userId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_apartment, menu);
        return true;
    }

    @Override
    public void bindComponents() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Advertisement");
        LinkedHashMap<Integer, String> data = new LinkedHashMap<>();
        data.put(0, "-- Select ad type --");
        data.put(AdType.SELL.val, AdType.SELL.name());
        data.put(AdType.RENT.val, AdType.RENT.name());
        generateSpinner(spnAdType, data);
        spnAdType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Map.Entry<Integer, String> item = (Map.Entry<Integer, String>) spnAdType.getSelectedItem();
                adType = item.getKey();
                if (adType == AdType.SELL.val) {
                    rdAvlFor.setVisibility(View.GONE);
                } else {
                    rdAvlFor.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tvCreateApartment.setOnClickListener(v -> createApartment());
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onDetach();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.createApt) {
            createApartment();
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    private void createApartment() {
        startActivity(new Intent(this, CreateApartmentActivity.class));
        overridePendingTransition(R.anim.activity_slide_left, R.anim.activity_slide_right);
    }


    @Override
    public void adCreated(Object o) {
        runOnUiThread(() -> {
            showMessage("Advertisement is created !");
            etAdTitle.setText("");
            spnAdType.setSelection(0);
            spnSelectApartment.setSelection(0);
        });

    }

    @Override
    public void aptCreated(int aptid) {

    }

    @Override
    public void onError(String message) {
        super.onError(message);
        runOnUiThread(() -> {
            showMessage(message);
        });
    }

    @Override
    public void setUserApartmentToView(List<Apartment> apartments) {
        LinkedHashMap<Integer, String> data = new LinkedHashMap<>();
        data.put(0, "-- Select your apartment --");
        for (Apartment apt : apartments) {
            data.put(apt.getAppartmentId(), apt.getTitle());
        }
        generateSpinner(spnSelectApartment, data);
        spnSelectApartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Map.Entry<Integer, String> item = (Map.Entry<Integer, String>) spnSelectApartment.getSelectedItem();
                selectedApartmentId = item.getKey();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void imageUploaded() {
        showMessage("Image Uploaded !");
    }


}
