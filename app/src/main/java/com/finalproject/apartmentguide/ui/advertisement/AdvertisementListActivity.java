package com.finalproject.apartmentguide.ui.advertisement;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.config.Constant;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.ui.advertisement.apartment_details.AdvertisementDetailsActivity;
import com.finalproject.apartmentguide.ui.advertisement.apartment_details.ApartmentDetailsActivity;
import com.finalproject.apartmentguide.ui.advertisement.apartment_details.AptAdapter;
import com.finalproject.apartmentguide.ui.base.BaseActivity;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

public class AdvertisementListActivity extends BaseActivity implements AdvertisementMvpView {
    private RecyclerView rcvAdvertisements;
    private ListView lvApartments;
    @Inject
    AdvertisementMvpPresenter<AdvertisementMvpView> presenter;
    @Inject
    PreferenceHelper preferenceHelper;
    private int APT = 2;
    private int AVT = 3;
    private int viewType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement_list);
        viewType = getIntent().getIntExtra("viewType", -1);
        APGApplication.getDefault().getApplicationComponent().inject(this);
        presenter.onAttach(this);
        initialize();
    }

    @Override
    public void initialize() {
        lvApartments = findViewById(R.id.lvApartments);
        rcvAdvertisements = findViewById(R.id.rcvAdvertisements);
        bindComponents();
    }

    @Override
    public void bindComponents() {
        getData();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(viewType == APT ? "Apartments" : "Advertisements");
    }

    private void getData() {
        if (viewType == AVT) {
            presenter.advertisementByUserId(preferenceHelper.getUserInformation().getUserId());
        } else if (viewType == APT) {
            presenter.getApartmentsByUserId(preferenceHelper.getUserInformation().getUserId());
        }
    }

    @Override
    public void setAdvertisementsToView(List<Advertisement> advertisements) {

    }

    @Override
    public void setAdvertisementsToUserView(List<Advertisement> advertisements) {
        runOnUiThread(() -> {
            rcvAdvertisements.setVisibility(View.VISIBLE);
            AdvertisementAdapter adapter = new AdvertisementAdapter(advertisements, this, true);
            rcvAdvertisements.setAdapter(adapter);
            rcvAdvertisements.setLayoutManager(new LinearLayoutManager(this));
            rcvAdvertisements.setItemAnimator(new DefaultItemAnimator());
            adapter.onItemClick(new AdvertisementAdapter.OnItemClick() {
                @Override
                public void onClick(int itemId) {
                    Intent intent = new Intent(AdvertisementListActivity.this, AdvertisementDetailsActivity.class);
                    intent.putExtra(Constant.INTENT_INT_EXTRA, itemId);
                    startActivity(intent);
                }

                @Override
                public void updateStatus(int adId, int status) {
                    presenter.updateStatus(adId, status);
                }
            });
        });
    }

    @Override
    public void noAdvertisementFound() {

    }

    @Override
    public void statusUpdated() {
        getData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return true;
    }

    @Override
    public void setApartmentsToUserView(List<Apartment> advertisements) {
        runOnUiThread(() -> {
            lvApartments.setVisibility(View.VISIBLE);
            AptAdapter aptAdapter = new AptAdapter(advertisements, this);
            lvApartments.setAdapter(aptAdapter);
            lvApartments.setOnItemClickListener((parent, view, position, id) -> {
                startActivity(new Intent(AdvertisementListActivity.this, ApartmentDetailsActivity.class).putExtra("apartmentDetails", new Gson().toJson(advertisements.get(position))));
            });
        });
    }
}
