package com.finalproject.apartmentguide.ui.advertisement;

import com.finalproject.apartmentguide.model.Advertisement;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.ui.base.MvpView;

import java.util.List;

public interface AdvertisementMvpView extends MvpView {
    void setAdvertisementsToView(List<Advertisement> advertisements);

    void setAdvertisementsToUserView(List<Advertisement> advertisements);

    void setApartmentsToUserView(List<Apartment> advertisements);

    void noAdvertisementFound();

    void statusUpdated();
}
