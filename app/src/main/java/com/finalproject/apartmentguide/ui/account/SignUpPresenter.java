package com.finalproject.apartmentguide.ui.account;

import android.annotation.SuppressLint;

import com.finalproject.apartmentguide.common.PreferenceHelper;
import com.finalproject.apartmentguide.common.PreferenceManager;
import com.finalproject.apartmentguide.core.ResponseParser;
import com.finalproject.apartmentguide.data.repository.UserRepository;
import com.finalproject.apartmentguide.model.Credentials;
import com.finalproject.apartmentguide.model.User;
import com.finalproject.apartmentguide.ui.base.BasePresenter;
import com.finalproject.apartmentguide.ui.base.MvpView;

import javax.inject.Inject;


public class SignUpPresenter<V extends SignUpMvpView> extends BasePresenter<V> implements SignUpMvpPresenter<V> {
    UserRepository repository;
    PreferenceHelper preferenceHelper;

    @Inject
    public SignUpPresenter(UserRepository repository, PreferenceManager preferenceManager) {
        this.repository = repository;
        this.preferenceHelper = preferenceManager;
    }

    @SuppressLint("CheckResult")
    @Override
    public void doSignUp(Credentials credentials) {
        getMvpView().showLoading();
        repository.registerUser(credentials).subscribe(loginResult -> {

            preferenceHelper.setUserInformation(loginResult.getUser());
            //preferenceHelper.setAccessToken(loginResult.getToken());
        }, throwable -> {
            getMvpView().hideLoading();
            getMvpView().onError(ResponseParser.getErrorMessageFromResponse(throwable));
        }, () -> {
            getMvpView().hideLoading();
            getMvpView().showMessage("Registration Completed !");
            getMvpView().signUpCompleted();
        });
    }
}
