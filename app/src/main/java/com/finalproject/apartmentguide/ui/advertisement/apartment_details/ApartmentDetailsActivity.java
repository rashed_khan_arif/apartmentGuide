package com.finalproject.apartmentguide.ui.advertisement.apartment_details;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.finalproject.apartmentguide.R;
import com.finalproject.apartmentguide.common.Util;
import com.finalproject.apartmentguide.config.ApiEndPoints;
import com.finalproject.apartmentguide.core.APGApplication;
import com.finalproject.apartmentguide.core.ResponseParser;
import com.finalproject.apartmentguide.data.repository.AdvertisementRepository;
import com.finalproject.apartmentguide.data.repository.ApartmentRepository;
import com.finalproject.apartmentguide.model.AdType;
import com.finalproject.apartmentguide.model.Apartment;
import com.finalproject.apartmentguide.model.ApartmentDetails;
import com.finalproject.apartmentguide.model.Attachments;
import com.finalproject.apartmentguide.model.Price;
import com.finalproject.apartmentguide.model.Review;
import com.finalproject.apartmentguide.ui.advertisement.post_advertisement.CreateApartmentActivity;
import com.finalproject.apartmentguide.ui.base.BaseActivity;
import com.google.gson.Gson;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ApartmentDetailsActivity extends BaseActivity {
    Apartment apartment;
    private TextView tvAptPrice, tvAptAddress, tvAptTitle,
            tvAptDetailsAddress, tvWaterCharge, tvGuardCharge, tvLiftCharge, tvElectricityCharges,
            tvOthersCharge, tvSize, tvRoom, tvKitchen, tvBath, tvCorridor, tvColor;
    private CardView cvPriceDetails, cvAptDetails;
    @Inject
    AdvertisementRepository advertisementRepository;
    private SliderLayout imageSlider;
    @Inject
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apartment_details);
        initialize();
        APGApplication.getDefault().getApplicationComponent().inject(this);
    }

    @Override
    public void initialize() {
        apartment = new Gson().fromJson(getIntent().getStringExtra("apartmentDetails"), Apartment.class);
        cvAptDetails = findViewById(R.id.cvAptDetails);
        cvPriceDetails = findViewById(R.id.cvPriceDetails);
        tvOthersCharge = findViewById(R.id.tvOthersCharge);
        tvElectricityCharges = findViewById(R.id.tvElectricityCharges);
        tvLiftCharge = findViewById(R.id.tvLiftCharge);
        tvGuardCharge = findViewById(R.id.tvGuardCharge);
        tvWaterCharge = findViewById(R.id.tvWaterCharge);
        tvAptDetailsAddress = findViewById(R.id.tvAptDetailsAddress);
        tvAptAddress = findViewById(R.id.tvAptAddress);
        tvAptPrice = findViewById(R.id.tvAptPrice);
        tvAptTitle = findViewById(R.id.tvAptTitle);
        tvSize = findViewById(R.id.tvSize);
        tvRoom = findViewById(R.id.tvRoom);
        tvKitchen = findViewById(R.id.tvKitchen);
        tvBath = findViewById(R.id.tvBath);
        tvCorridor = findViewById(R.id.tvCorridor);
        tvColor = findViewById(R.id.tvColor);
        imageSlider = findViewById(R.id.imageSlider);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Apartment");
        bindComponents();
    }

    @Override
    public void bindComponents() {
        imageSlider.setIndicatorAnimation(SliderLayout.Animations.FILL); //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        imageSlider.setScrollTimeInSec(1); //set scroll delay in seconds :

        if (apartment != null) {
            if (apartment.getTitle() != null) {
                tvAptTitle.setText(apartment.getTitle());
            }
            if (apartment.getLocation() != null) {
                tvAptAddress.setText(apartment.getLocation().getLocationName());
                tvAptDetailsAddress.setText(apartment.getLocation().getDetailsAddress());
            }
            Price price = apartment.getPrice();
            if (price != null) {
                tvAptPrice.setText(String.valueOf(price.getAmount()));
                if (price.getPriceDetails() != null) {
                    tvWaterCharge.setText(String.valueOf(price.getPriceDetails().getWaterCharge()));
                    tvElectricityCharges.setText(String.valueOf(price.getPriceDetails().getElectricityCharge()));
                    tvGuardCharge.setText(String.valueOf(price.getPriceDetails().getGaurdCharge()));
                    tvLiftCharge.setText(String.valueOf(price.getPriceDetails().getLiftCharge()));
                    tvOthersCharge.setText(String.valueOf(price.getPriceDetails().getOthersCharge()));
                } else cvPriceDetails.setVisibility(View.GONE);
            }
            if (apartment.getAttachments() != null) {
                for (Attachments attachments : apartment.getAttachments()) {
                    //attachments.isTitle()
                    String url = ApiEndPoints.getApartmentImageUrl(attachments.getFileName());
                    SliderView sliderView = new SliderView(this);
                    sliderView.setImageUrl(url);
                    sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
                    //sliderView.setDescription("setDescription " + (i + 1));
                    imageSlider.addSliderView(sliderView);
                }
            } else {
                imageSlider.setVisibility(View.GONE);
            }
            if (apartment.getApartmentDetails() != null) {
                ApartmentDetails details = apartment.getApartmentDetails();
                tvSize.setText(details.getSize());
                tvRoom.setText(String.valueOf(details.getRoom()));
                tvBath.setText(String.valueOf(details.getBath()));
                tvKitchen.setText(String.valueOf(details.getKitchen()));
                tvCorridor.setText(String.valueOf(details.getCorridor()));
                tvColor.setText(details.getColor());
            } else cvAptDetails.setVisibility(View.GONE);

        }

    }


    private void showUserReviews(List<Review> reviews) {
        if (reviews.size() == 0) {
            showMessage("No Review Found For This Apartment !");
            return;
        }
        Dialog dialog = new Dialog(this, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
        View v = LayoutInflater.from(this).inflate(R.layout.read_reviews_layout, null);
        dialog.setContentView(v);
        dialog.show();
        ListView lvReadReviews = v.findViewById(R.id.lvReadReviews);
        ReviewAdapter ra = new ReviewAdapter(reviews, this);
        lvReadReviews.setAdapter(ra);
    }

    public void readReview(View view) {
        if (apartment != null)
            advertisementRepository.getReviewsByAptId(apartment.getAppartmentId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(reviews -> showUserReviews(reviews), throwable -> showMessage(ResponseParser.getErrorMessageFromResponse(throwable)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_apt, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.editApt) {
            if (apartment != null)
                startActivity(new Intent(this, CreateApartmentActivity.class).putExtra("apt", gson.toJson(apartment)));
            return true;
        }
        return true;
    }
}
